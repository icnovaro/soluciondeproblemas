<?php 
include_once './php/DAO/usuarioDAO.php';
include_once './php/DAO/conectionDAO.php';
include_once './php/Entities/Usuario.php';

$usuarioDAO= new UsuarioDAO();
?>
<header>
  <div class="row-1">
    <div class="container_12">
      <div class="grid_12">
        <div class="logo fleft">
          <h1><a href="index.php">Terapia en Soluciones de Problemas ON-LINE</a></h1>
        </div>
        <div class="login">
          <ul>
            <?php
                  if(isset($_SESSION['user'])){
                 
                   $usuario=$usuarioDAO->getUserById($_SESSION['user']);
                   echo '<li><a href="user.php">Bienvenido: '.$usuario->getNombres().'</a></li>';
                   if($usuario->getIsAdmin()==1){
                      echo "<li><a href='codigos.php'>Códigos</a></li>"; 
                   }  
                   echo '<li><a href="php/closeDO.php">Cerrar Sesión</a></li>';  
                  
                  }else{
                      echo '<li><a href="login.php">Login</a></li>
                            <li><a href="registro.php">Registro</a></li>';
                  }
            ?>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="row-3">
    <div class="container_12">
      <div class="grid_12">
        <!-- menu -->
        <nav id="main-nav-menu">
          <ul class="sf-menu">
            <li class="active"><a href="index.php">Inicio</a></li>
            <?php
                  if(isset($_SESSION['user'])){
                      echo "<li><a href='fases.php'>Fases</a></li>";
                  }
            ?>
            <li><a href="acercade.php">Acerca de </a>
              <ul>
                <li><a href="autor.php">Autor</a></li>
		<li><a href="objetivo.php">Público objetivo</a></li>
              </ul>
            </li>
            <li><a href="contacto.php">Contacto</a></li>
          </ul>
        </nav>
        <!-- end menu -->
        <select id="responsive-main-nav-menu" onChange="javascript:window.location.replace(this.value);">
        </select>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</header>


