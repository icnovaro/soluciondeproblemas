<?php
include_once ('DAO/conectionDAO.php');
include_once ('DAO/codigoDAO.php');
include_once ('DAO/usuarioDAO.php');
include_once ('DAO/situacionDAO.php');
include_once ('DAO/subproblemaDAO.php');
include_once ('DAO/alternativaDAO.php');
include_once ('Entities/Subproblema.php');
include_once ('Entities/Alternativa.php');
include_once ('Entities/Situacion.php');
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}
$idUsuario= $_SESSION['user'];
// poner isEnabled=0 y isEnded=0 en la situacion que lo tenga
$situacionDAO= new situacionDAO();
$idSituacion= $situacionDAO->getSelectedSituacion($idUsuario)->getId();
$situacionDAO->reset($idUsuario);
// traer los subproblemas
$subproblemaDAO = new subproblemaDAO();
$subproblemas= $subproblemaDAO->getSubproblemasBySituacion($idSituacion);

$alternativaDAO= new alternativaDAO();

foreach ($subproblemas as $subproblema){
    // eliminar todas las alternativas asociadas a un subproblema
    $alternativaDAO->delete($subproblema->getId());
    $subproblemaDAO->delete($subproblema->getId());
}
$usuarioDAO=new UsuarioDAO();
$usuarioDAO->reiniciar($idUsuario);;

header("location: ./../fase.php?id=2");
exit;

?>
