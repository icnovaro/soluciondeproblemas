<?php
include_once ('Entities/Usuario.php');
include_once ('DAO/conectionDAO.php');
include_once ('DAO/codigoDAO.php');
include_once ('DAO/usuarioDAO.php');
include_once ('DAO/usuarioDAO.php');


$usuario1=$_POST['usuario'];
$email=$_POST['email'];
$usuarioDAO= new UsuarioDAO();


//Verify errors
if($usuario1 == "" && $email == ""){
    header("location: ./../restauracion.php?error0");
    exit;
}

$usuario = new Usuario();

if($usuario1!=""){
    //Buscar usuario
    if($usuarioDAO->userExists($usuario1)==false){
       header("location: ./../restauracion.php?error1");
       exit;
    }
    else{
        $usuario=$usuarioDAO->getUserByName($usuario1);
    }
}else{
    //buscar email
    if($usuarioDAO->emailExists($email)==false){
       header("location: ./../restauracion.php?error1");
       exit;
    }else{
        $usuario=$usuarioDAO->getUserByEmail($email);
    }
    //si no está lanzar error1
}
$nuevaClave=randomPassword();

$to = $usuario->getEmail();
 $subject = "Nueva Clave para Terapia de Problemas Online";
 $body = "Hola, tu nueva clave es:".$nuevaClave." te recomendamos que entres a nuestra plataforma y la cambies cuando puedas.";
 if (mail($to, $subject, $body)) {
      header("location: ./../restauracion.php?ok");
      $usuarioDAO->updateClave($nuevaClave, $usuario->getId());
      exit;
  } else {
      header("location: ./../restauracion.php?error2");
      exit;
  }




function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
?>
