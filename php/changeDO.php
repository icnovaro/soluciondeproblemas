<?php
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}

include_once ('DAO/conectionDAO.php');
include_once ('DAO/usuarioDAO.php');
include_once ('Entities/Usuario.php');

$actual = $_POST['actual'];
$nueva1 = $_POST['nueva1']; 
$nueva2 = $_POST['nueva2'];

$idUsuario= $_SESSION['user'];


$usuarioDAO = new UsuarioDAO();
$actualDB=$usuarioDAO->getClave($idUsuario);

if(md5($actual) != $actualDB){
    header("location: ./../user.php?err4");
    exit;
}

if($actual=="" || $nueva1=="" || $nueva2==""){
    header("location: ./../user.php?err1");
    exit;
}
if($nueva1!=$nueva2){
    header("location: ./../user.php?err2");
    exit;
}
if(strlen($nueva1)<7 || strlen($nueva2)<7){
    header("location: ./../user.php?err3");
    exit;
}


$usuarioDAO->updateClave($nueva1, $idUsuario);

header("location: ./../user.php?s");
exit;

?>
