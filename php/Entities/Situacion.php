<?php

class Situacion {
    
    private $id;
    private $nombre;
    private $descripcion;
    private $id_usuario;
    private $enable;
    private $isEnded;
    private $sucedio;
    private $cambios;
    private $consecuencias;
    private $orden;

    
    public function __construct() {
     $this->id=0;
     $this->nombre="";
     $this->descripcion="";
     $this->id_usuario="";
     $this->isEnded=0;
     $this->sucedio="";
     $this->cambios="";
     $this->consecuencias="";
     $this->orden="";
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setNombre($nombre){
        $this->nombre=$nombre;
    }
    public function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setIdUsuario($idUsuario){
        $this->id_usuario=$idUsuario;
    }
    public function getIdUsuario(){
        return $this->id_usuario;
    }
    public function setEnabled($enable){
        $this->enable=$enable;
    }
    public function getEnabled(){
        return $this->enable;
    }
    public function setisEnded($isEnded){
        $this->isEnded=$isEnded;
    }
    public function getisEnded(){
        return $this->isEnded;
    }
    public function setSucedio($sucedio){
        $this->sucedio=$sucedio;
    }
    public function getSucedio(){
        return $this->sucedio;
    }
    public function setCambios($cambios){
        $this->cambios=$cambios;
    }
    public function getCambios(){
        return $this->cambios;
    }
    public function setConsecuencias($consecuencias){
        $this->consecuencias=$consecuencias;
    }
    public function getConsecuencias(){
        return $this->consecuencias;
    }
    public function setOrden($orden){
        $this->orden=$orden;
    }
    public function getOrden(){
        return $this->orden;
    }
}
?>
