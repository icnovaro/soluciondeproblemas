<?php
class Encuesta{
    
    private $id;
    private $titulo;
    
    public function __construct() {
     $this->id=0;
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function setTitulo($titulo){
        $this->titulo=$titulo;
    }
    
}
?>