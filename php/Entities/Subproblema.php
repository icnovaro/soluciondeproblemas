<?php

class Subproblema{
    
    private $id;
    private $descripcion;
    private $objetivo;
    private $obstaculo;
    private $idSituacion;
    private $autoregistro;
    private $aportes;
    
    public function __construct() {
     $this->id=0;
     $this->descripcion="";
     $this->objetivo="";
     $this->obstaculo="";
     $this->idSituacion="";
     $this->autoregistro="";
     $this->aportes="";
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    public function setObjetivo($objetivo){
        $this->objetivo=$objetivo;
    }
    public function getObjetivo(){
        return $this->objetivo;
    }
    public function setObstaculo($obstaculo){
        $this->obstaculo=$obstaculo;
    }
    public function getObstaculo(){
        return $this->obstaculo;
    }
    public function setIdSituacion($idSituacion){
        $this->idSituacion=$idSituacion;
    }
    public function getIdSituacion(){
        return $this->idSituacion;
    }
    public function setAutoregistro($autoregistro){
        $this->autoregistro=$autoregistro;
    }
    public function getAutoregistro(){
        return $this->autoregistro;
    }
    public function setAportes($aportes){
        $this->aportes=$aportes;
    }
    public function getAportes(){
        return $this->aportes;
    }
}

?>
