<?php
class Opcion{
    
    private $id;
    private $texto;
    
    public function __construct() {
     $this->id=0;
     $this->texto ="";
    }
    public function getId(){
        return $this->id;
    }
    public function setID($id){
        $this->id=$id;
    }
    public function getTexto(){
        return $this->texto;
    }
    public function setTexto($texto){
        $this->texto=$texto;
    }
}
?>