<?php

class Alternativa{
    
    private $id;
    private $descripcion;
    private $consecuencia;
    private $valoracion;
    private $probabilidad;
    private $idSubProblema;
    
    public function __construct() {
     $this->id=0;
     $this->descripcion="";
     $this->consecuencia="";
     $this->valoracion=0;
     $this->probabilidad=0;
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    public function setConsecuencia($consecuencia){
        $this->consecuencia=$consecuencia;
    }
    public function getConsecuencia(){
        return $this->consecuencia;
    }
    public function setValoracion($valoracion){
        $this->valoracion=$valoracion;
    }
    public function getValoracion(){
        return $this->valoracion;
    }
    public function setProbabilidad($probabilidad){
        $this->probabilidad=$probabilidad;
    }
    public function getProbabilidad(){
        return $this->probabilidad;
    }
    public function setIdSubproblema($idSubproblema){
        $this->idSubProblema=$idSubproblema;
    }
    public function getIdSubproblema(){
        return $this->idSubProblema;
    }
}

?>
