<?php
class Usuario{
    
    private $id;
    private $nombres;
    private $email;
    private $usuario;
    private $clave;
    private $isAdmin;
    
    public function __construct() {
     $this->id=0;
    }
    public function getId(){
        return $this->id;
    }
    public function setID($id){
        $this->id=$id;
    }
    public function getNombres(){
        return $this->nombres;
    }
    public function setNombres($nombres){
        $this->nombres=$nombres;
    }
    public function getEmail(){
        return $this->email;
    }
    public function setEmail($email){
        $this->email=$email;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setUsuario($usuario){
        $this->usuario=$usuario;
    }
    public function getClave(){
        return $this->clave;
    }
    public function setClave($clave){
        $this->clave=$clave;
    }
    public function getIsAdmin(){
        return $this->isAdmin;
    }
    public function setIsAdmin($isAdmin){
        $this->isAdmin=$isAdmin;
    }
}
?>