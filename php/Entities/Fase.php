<?php

class Fase {
    
    private $id;
    private $numero;
    private $titulo;
    private $descripcion;
    private $imagen;
    
    public function __construct() {
     $this->id=0;
    }
    public function getId(){
        return $this->id;
    }
    public function setID($id){
        $this->id=$id;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setNumero($numero){
        $this->numero=$numero;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function setTitulo($titulo){
        $this->titulo=$titulo;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setDescripcion($descripcion){
        $this->descripcion=$descripcion;
    }
    public function getImagen(){
        return $this->imagen;
    }
    public function setImagen($imagen){
        $this->imagen=$imagen;
    }
}

?>
