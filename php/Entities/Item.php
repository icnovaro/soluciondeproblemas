<?php

class Item {
    
    private $id;
    private $numero;
    private $titulo;
    private $idFase;
    private $tipo;
    
    public function __construct() {
     $this->id=0;
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setNumero($numero){
        $this->numero=$numero;
    }
    public function getTitulo(){
        return $this->titulo;
    }
    public function setTitulo($titulo){
        $this->titulo=$titulo;
    }
    public function getIdFase(){
        return $this->idFase;
    }
    public function setIdFase($idFase){
        $this->idFase=$idFase;
    }
    public function getTipo(){
        return $this->tipo;
    }
    public function setTipo($tipo){
        $this->tipo=$tipo;
    }
    
}

?>
