<?php
class Pregunta{
    
    private $id;
    private $texto;
    private $opciones;
    
    public function __construct() {
     $this->id=0;
     $this->texto ="";
     $this->opciones = array ();
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getTexto(){
        return $this->texto;
    }
    public function setTexto($texto){
        $this->texto=$texto;
    }
    
    public function getOpciones(){
    	return $this->opciones;
    }
	public function setOpciones($opciones){
		$this->opciones = $opciones;
	}
}
?>