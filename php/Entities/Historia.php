<?php

class Historia {
    
    private $id;
    private $id_usuario;
    private $desc;
    private $desc1;
    private $desc2;
    private $desc3;
    
    public function __construct() {
     $this->id=0;
     $this->desc="";
     $this->desc1="";
     $this->desc2="";
     $this->desc3="";
    }
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getIdUsuario(){
        return $this->id_usuario;
    }
    public function setIdUsuario($id_usuario){
        $this->id_usuario=$id_usuario;
    }
    public function setDesc($desc){
        $this->desc=$desc;
    }
    public function getDesc(){
        return $this->desc;
    }
    public function setDesc1($desc1){
        $this->desc1=$desc1;
    }
    public function getDesc1(){
        return $this->desc1;
    }
    public function setDesc2($desc2){
        $this->desc2=$desc2;
    }
    public function getDesc2(){
        return $this->desc2;
    }
    public function setDesc3($desc3){
        $this->desc3=$desc3;
    }
    public function getDesc3(){
        return $this->desc3;
    }
}

?>
