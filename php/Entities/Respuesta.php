<?php
class Respuesta{
    
    private $id;
    private $idUsuarioEncuesta;
    private $idPregunta;
    private $puntaje;
    
    public function __construct() {
     $this->id = 0;
     $this->idUsuarioEncuesta = 0;
     $this->idPregunta = 0;
     $this->puntaje = 0;
    }
    
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id=$id;
    }
    public function getUsuarioEncuesta(){
        return $this->idUsuarioEncuesta;
    }
    public function setUsuarioEncuesta($idUsuarioEncuesta){
        $this->idUsuarioEncuesta=$idUsuarioEncuesta;
    }
    public function getIdPregunta(){
        return $this->idPregunta;
    }
    public function setIdPregunta($idPregunta){
        $this->idPregunta=$idPregunta;
    }
    public function getPuntaje(){
        return $this->puntaje;
    }
    public function setPuntaje($puntaje){
        $this->puntaje=$puntaje;
    }
    
}
?>