<?php
class UsuarioEncuesta{
    
    private $id;
    private $id_encuesta;
    private $id_usuario;
    private $fecha;
    
     public function __construct() {
     $this->id = 0;
     $this->id_encuesta = 0;
     $this->id_usuario = 0;
     $this->fecha="";
    }
    
    /**
    * Setters and Getters
    **/
    
    public function setId($id){
        $this->id=$id;
    }
    public function getId(){
        return $this->id;
    }
    
    public function setEncuestaId($id_encuesta){
        $this->id_encuesta=$id_encuesta;
    }
    
	public function getEncuestaId(){
        return $this->id_encuesta;
    }
   	
   	public function setUsuarioId($id_usuario){
        $this->id_usuario=$id_usuario;
    }
    
	public function getUsuarioId(){
        return $this->id_usuario;
    }
    
    public function setFecha($fecha){
        $this->fecha=$fecha;
    }
    
	public function getFecha(){
        return $this->fecha;
    }
   	
    
}
?>