<?php

class respuestaDAO {
    
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function save($respuesta){
        
        $nuevaRespuesta = $respuesta;
        $querty =   "insert into respuesta
                    (id_usuario_encuesta, id_pregunta, puntaje)
                    values(
                    \"".mysql_real_escape_string($nuevaRespuesta->getUsuarioEncuesta())."\",
                    \"".mysql_real_escape_string($nuevaRespuesta->getIdPregunta())."\",
                    \"".mysql_real_escape_string($nuevaRespuesta->getPuntaje())."\"
                    )";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (save Respuesta): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function update($respuesta){
        
        $querty =   "UPDATE respuesta SET 
                    id_usuario_encuesta = '".$respuesta->getUsuarioEncuesta()."',
                    id_pregunta= ".$respuesta->getIdPregunta().",
                    puntaje = '".$respuesta->getPuntaje()."'
                    WHERE id=".$respuesta->getId();
        
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (update Respuesta): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function getRespuesta($idPregunta, $idEncuesta){
        
    	$sql="SELECT * FROM respuesta WHERE id_pregunta=".$idPregunta." AND id_usuario_encuesta=".$idEncuesta;
        
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
       	
       	$respuesta = new Respuesta();
        if($numregistros == 0){
            return null;
        }
        
        $respuesta->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $respuesta->setUsuarioEncuesta($this->conexionDAO->ObjetoConsulta2[0][1]);
        $respuesta->setIdPregunta($this->conexionDAO->ObjetoConsulta2[0][2]);
        $respuesta->setIdPuntaje($this->conexionDAO->ObjetoConsulta2[0][3]);
        
        return $respuesta;
    	
    }
    
    function getRespuestasByUsuarioEncuesta($usuarioEncuestaID){
    	$sql="SELECT * FROM respuesta WHERE id_usuario_encuesta=".$usuarioEncuestaID;
    	
    	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        $lista=array();

        if($numregistros == 0){
            return $lista;
        }
        for($i = 0; $i < $numregistros ; $i++){
        
            $respuesta = new Respuesta(); 
            $respuesta->setId($this->conexionDAO->ObjetoConsulta2[$i][0]);
            $respuesta->setUsuarioEncuesta($this->conexionDAO->ObjetoConsulta2[$i][1]);
            $respuesta->setIdPregunta($this->conexionDAO->ObjetoConsulta2[$i][2]);
            $respuesta->setIdPuntaje($this->conexionDAO->ObjetoConsulta2[$i][3]);
            $lista[$i] = $respuesta;
            
        }
        return $lista;
    }
}

?>
