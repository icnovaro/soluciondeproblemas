<?php

class faseDAO {

    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function getFasesByUsuario($idUser){
        $sql="SELECT * FROM fase WHERE numero IN (SELECT id_fase FROM usuario_fase WHERE id_usuario = ".$idUser.")";
        
        //$sql = 'SELECT * from usuario_fase where id_usuario = "'.mysql_real_escape_string($idUser).'"';
        //echo $sql;
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $fase = new Fase(); 
            $fase->setId($this->conexionDAO->ObjetoConsulta2[$i][0]);
            $fase->setNumero($this->conexionDAO->ObjetoConsulta2[$i][1]);
            $fase->setTitulo($this->conexionDAO->ObjetoConsulta2[$i][2]);
            $fase->setDescripcion($this->conexionDAO->ObjetoConsulta2[$i][3]);
            $fase->setImagen($this->conexionDAO->ObjetoConsulta2[$i][4]);
            $lista[$i] = $fase;
        }
        
        return $lista;
        
    }
    
    function isEnabled($idUsuario, $idFase){
        
        $sql="SELECT estado FROM usuario_fase WHERE id_usuario='".$idUsuario."' AND id_fase='".$idFase."'";
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        if($numregistros == 0){
            return null;
        }

        return $this->conexionDAO->ObjetoConsulta2[0][0];
    }
    
    function getFaseById($id){
        
        $sql="SELECT * FROM fase WHERE numero=".$id;
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $fase = new Fase(); 
        if($numregistros == 0){
            return $fase;
        }

        $fase->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $fase->setNumero($this->conexionDAO->ObjetoConsulta2[0][1]);
        $fase->setTitulo($this->conexionDAO->ObjetoConsulta2[0][2]);
        $fase->setDescripcion($this->conexionDAO->ObjetoConsulta2[0][3]);
        $fase->setImagen($this->conexionDAO->ObjetoConsulta2[0][4]);
        
        return $fase; 
        
    }
    
    function isCompleted($idUsuario, $idFase){
        $sql="SELECT * FROM usuario_item WHERE id_usuario=".$idUsuario." AND id_fase=".$idFase;
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $lista=array();
        if($numregistros == 0){
            return $lista;
        }
        for($i = 0; $i < $numregistros ; $i++){
            if($this->conexionDAO->ObjetoConsulta2[$i][3]==0)
                return false;
        }
        $sql2="UPDATE usuario_fase SET estado=1 WHERE id_usuario=".$idUsuario." AND id_fase=".$idFase;
        $result = mysql_query($sql2, $this->conexionDAO->Conexion_ID);
        
	if (!$result){
            echo 'Ooops (updateFase): '.mysql_error();
            return false;
        }
        
        return true;
    }
    
    function getLastFase($idUsuario){
        $sql="SELECT id_fase FROM usuario_fase WHERE id_usuario='".$idUsuario."' AND estado='0' limit 1";
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
       
        if($numregistros == 0){
            return null;
        }
        $fase= $this->getFaseById($this->conexionDAO->ObjetoConsulta2[0][0]);
        
        return $fase;
        
    }
}

?>
