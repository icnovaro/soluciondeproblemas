<?php

class alternativaDAO {

    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function save($alternativa){
        $querty =   "insert into alternativa
                    (descripcion, consecuencia, valoracion, probabilidad, id_subproblema)
                    values(
                    \"".mysql_real_escape_string($alternativa->getDescripcion())."\",
                    \"".mysql_real_escape_string($alternativa->getConsecuencia())."\",
                    \"".mysql_real_escape_string($alternativa->getValoracion())."\",
                    \"".mysql_real_escape_string($alternativa->getProbabilidad())."\",
                    \"".mysql_real_escape_string($alternativa->getIdSubproblema())."\"
                    )";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveAlternativa): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function getAlternativasBySubproblema($subproblemaId){
        
        $sql="SELECT * FROM alternativa WHERE id_subproblema=".$subproblemaId;
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $alternativa = new Alternativa(); 
            $alternativa->setId($this->conexionDAO->ObjetoConsulta2[$i][0]);
            $alternativa->setDescripcion($this->conexionDAO->ObjetoConsulta2[$i][1]);
            $alternativa->setConsecuencia($this->conexionDAO->ObjetoConsulta2[$i][2]);
            $alternativa->setValoracion($this->conexionDAO->ObjetoConsulta2[$i][3]);
            $alternativa->setProbabilidad($this->conexionDAO->ObjetoConsulta2[$i][4]);
            $alternativa->setIdSubproblema($this->conexionDAO->ObjetoConsulta2[$i][5]);
            $lista[$i] = $alternativa;
        }
        
        return $lista;
        
    }
    
    function getSelectedSituacion($idUsuario){
        $sql="SELECT * FROM situacion WHERE id_usuario=".$idUsuario." AND enable=1";
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $situacion = new Situacion();
        if($numregistros == 0){
            return $historia;
        }
        
        $situacion->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $situacion->setNombre($this->conexionDAO->ObjetoConsulta2[0][1]);
        $situacion->setDescripcion($this->conexionDAO->ObjetoConsulta2[0][2]);
        $situacion->setIdUsuario($this->conexionDAO->ObjetoConsulta2[0][3]);
        $situacion->setEnabled($this->conexionDAO->ObjetoConsulta2[0][4]);
        
        return $situacion; 
    }
    
    function setDescripcion($idSituacion,$descripcion){
        $querty =   "UPDATE situacion SET
                     descripcion =
                    \"".mysql_real_escape_string($descripcion)."\"
                    WHERE id =
                    ".mysql_real_escape_string($idSituacion)."
                    ";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	//echo $querty;	
        if (!$result){
            echo 'Ooops (updatetema): '.mysql_error();
            return false;
        }

        return true;
        
    }
    
    function getValoracion($value){
        switch ($value){
            case 1:
                return "-";
                break;
            case 2:
                return "+";
                break;
            case 3:
                return "++";
                break;
        }
    }
    
    function getProbabilidad($value){
        switch ($value){
            case 1:
                return "PP";
                break;
            case 2:
                return "P";
                break;
            case 3:
                return "MP";
                break;
        }
    }
    function delete($idSubproblema){
        
        
        
        $sql = "DELETE FROM alternativa WHERE id_subproblema=".$idSubproblema;
        
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
         //echo $querty;	
         if (!$result){
             echo 'Ooops (Delete alternativa): '.mysql_error();
             return false;
         }

        return true;
    }
}

?>
