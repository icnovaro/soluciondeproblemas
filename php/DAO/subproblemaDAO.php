<?php

class subproblemaDAO {

    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function save($subproblema){
        $querty =   "insert into subproblema
                    (descripcion, objetivo, obstaculo,id_situacion,autoregistro,aportes)
                    values(
                    \"".mysql_real_escape_string($subproblema->getDescripcion())."\",
                    \"".mysql_real_escape_string($subproblema->getObjetivo())."\",
                    \"".mysql_real_escape_string($subproblema->getObstaculo())."\",
                    \"".mysql_real_escape_string($subproblema->getIdSituacion())."\",
                    \"".mysql_real_escape_string($subproblema->getAutoregistro())."\",
                    \"".mysql_real_escape_string($subproblema->getAportes())."\"
                    )";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveSubproblema): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function update($subproblema){
        
        $querty =   "UPDATE subproblema SET 
                    descripcion = \"".mysql_real_escape_string($subproblema->getDescripcion())."\",
                    objetivo = \"".mysql_real_escape_string($subproblema->getObjetivo())."\",
                    obstaculo= \"".mysql_real_escape_string($subproblema->getObstaculo())."\"
                    WHERE id=".$subproblema->getId();
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveSubproblema): '.mysql_error();
            return false;
        }
        return true;
    }
    function updateAutoregistro($id,$autoregistro){
        $sql="UPDATE subproblema SET autoregistro='".$autoregistro."' WHERE id=".$id;
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (updateSubproblema): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function updateAportes($id,$aportes){
        $sql="UPDATE subproblema SET aportes='".$aportes."' WHERE id=".$id;
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        if (!$result){
            //Mensaje de error
            echo 'Ooops (updateSubproblema): '.mysql_error();
            return false;
        }
        return true;
    }
    
    
    function getSubproblemasBySituacion($idSituacion){
        $sql="SELECT * FROM subproblema WHERE id_situacion=".$idSituacion;
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $subproblema = new Subproblema(); 
            $subproblema->setId($this->conexionDAO->ObjetoConsulta2[$i][0]);
            $subproblema->setDescripcion($this->conexionDAO->ObjetoConsulta2[$i][1]);
            $subproblema->setObjetivo($this->conexionDAO->ObjetoConsulta2[$i][2]);
            $subproblema->setObstaculo($this->conexionDAO->ObjetoConsulta2[$i][3]);
            $subproblema->setIdSituacion($this->conexionDAO->ObjetoConsulta2[$i][4]);
            $subproblema->setAutoregistro($this->conexionDAO->ObjetoConsulta2[$i][5]);
            $subproblema->setAportes($this->conexionDAO->ObjetoConsulta2[$i][6]);
            $lista[$i] = $subproblema;
        }
        
        return $lista;
    }
    
    function delete($idSubproblema){
        
        
        
        $sql = "DELETE FROM subproblema WHERE id=".$idSubproblema;
        
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
         //echo $querty;	
         if (!$result){
             echo 'Ooops (Delete subproblema): '.mysql_error();
             return false;
         }

        return true;
    }

}

?>
