<?php
class UsuarioDAO{
    
    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function save($nombres,$usuario,$clave,$email, $codigo){
        $querty =   "insert into usuario
                    (nombres, email, usuario, clave)
                    values(
                    \"".mysql_real_escape_string($nombres)."\",
                    \"".mysql_real_escape_string($email)."\",
                    \"".mysql_real_escape_string($usuario)."\",
                    \"".mysql_real_escape_string(md5($clave))."\"
                    )";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveUsuario): '.mysql_error();
            return false;
        }
        
        $this->deleteCode($codigo);
        $this->fillFases($this->getIdByUsername($usuario));
        return true;
    }
    
    function saveCode($codigo){
        $sql ="insert into claves (codigo) values('".$codigo."')";
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveUsuario): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function getCodes(){
        
        $sql = 'SELECT codigo from claves';
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        
        $codigos= array();
        
        if($numregistros == 0){
            return null;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $codigos[$i] = $this->conexionDAO->ObjetoConsulta2[$i][0];
        }
        
        return $codigos;
    }
    
    function updateClave($clave, $idUsuario){
        $sql="UPDATE usuario SET clave='".  md5($clave)."' WHERE id=".$idUsuario;
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        if (!$result){
            //Mensaje de error
            echo 'Ooops (updateClave): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function getClave($idUsuario){
        $sql = "SELECT clave FROM usuario WHERE id=".$idUsuario;
        
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        
        if($numregistros == 0){
            return null;
        }
        return $this->conexionDAO->ObjetoConsulta2[0][0];
    }
    
    function userExists($nombreUsuario){
        $sql = "SELECT * FROM usuario WHERE usuario='".$nombreUsuario."'";
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        
        if($numregistros == 0){
            return false;
        }
        return true;
    }
    
    function emailExists($email){
        $sql = "SELECT * FROM usuario WHERE email='".$email."'";
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        
        if($numregistros == 0){
            return false;
        }
        return true;
    }
    
    function deleteCode($codigo){
        $sql="DELETE FROM claves WHERE codigo='".$codigo."'";
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        if (!$result){
            //Mensaje de error
            echo 'Ooops (deleteClave): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function reciclarProceso($idUsuario){
        $sql="UPDATE usuario_item SET estado=0 WHERE (id_fase=3 OR id_fase=4 OR id_fase=5 OR id_fase=6) AND id_usuario=".$idUsuario." AND 1=(SELECT tipo FROM item WHERE usuario_item.id_item=item.numero AND usuario_item.id_fase=item.id_fase)";

        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        
        if (!$result){
            //Mensaje de error
            echo 'Ooops (Reiniciar Proceso): '.mysql_error();
            return false;
        }
        
       $sql2="UPDATE usuario_fase SET estado=0 WHERE (id_fase=3 OR id_fase=4 OR id_fase=5 OR id_fase=6) AND id_usuario=".$idUsuario;
        
        $result2 = mysql_query($sql2, $this->conexionDAO->Conexion_ID);
        
        if (!$result2){
            //Mensaje de error
            echo 'Ooops (Reciclar Proceso): '.mysql_error();
            return false;
        }
        return true;

        
    }
    
    function reiniciar($idUsuario){
        $sql="UPDATE usuario_item SET estado=0 WHERE (id_fase=2 OR id_fase=3 OR id_fase=4 OR id_fase=5 OR id_fase=6) AND id_usuario=".$idUsuario." AND 1=(SELECT tipo FROM item WHERE usuario_item.id_item=item.numero AND usuario_item.id_fase=item.id_fase)";

        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        
        if (!$result){
            //Mensaje de error
            echo 'Ooops (Reiniciar Proceso): '.mysql_error();
            return false;
        }
        
        $sql2="UPDATE usuario_fase SET estado=0 WHERE (id_fase=2 OR id_fase=3 OR id_fase=4 OR id_fase=5 OR id_fase=6) AND id_usuario=".$idUsuario;
        
        $result2 = mysql_query($sql2, $this->conexionDAO->Conexion_ID);
        
        if (!$result2){
            //Mensaje de error
            echo 'Ooops (Reiniciar Proceso): '.mysql_error();
            return false;
        }
        return true;
    }
    
    /**
    * Fill user's state phases and it's items
    * i= start phase
    * @param: $idUsuario -> user's id
    **/
    
    function fillFases($idUsuario){
        for($i=0;$i<7;$i++){
             $sql="INSERT INTO usuario_fase (id_fase,id_usuario,estado) values(".$i.",".$idUsuario.",0)";
             $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        }
        
        $sql2="INSERT INTO usuario_item
                (id_usuario, id_item, id_fase, estado)
                VALUES
                  (".$idUsuario.", 1, 0, 0),
                  (".$idUsuario.", 1, 1, 0),
                  (".$idUsuario.", 2, 1, 0),
                  (".$idUsuario.", 3, 1, 0),
                  (".$idUsuario.", 4, 1, 0),
                  (".$idUsuario.", 5, 1, 0),
                  (".$idUsuario.", 6, 1, 0),
                  (".$idUsuario.", 1, 2, 0),
                  (".$idUsuario.", 2, 2, 0),
                  (".$idUsuario.", 3, 2, 0),
                  (".$idUsuario.", 4, 2, 0),
                  (".$idUsuario.", 5, 2, 0),
                  (".$idUsuario.", 1, 3, 0),
                  (".$idUsuario.", 2, 3, 0),
                  (".$idUsuario.", 3, 3, 0),
                  (".$idUsuario.", 4, 3, 0),
                  (".$idUsuario.", 5, 3, 0),
                  (".$idUsuario.", 1, 4, 0),
                  (".$idUsuario.", 2, 4, 0),
                  (".$idUsuario.", 3, 4, 0),
                  (".$idUsuario.", 4, 4, 0),
                  (".$idUsuario.", 5, 4, 0),
                  (".$idUsuario.", 1, 5, 0),
                  (".$idUsuario.", 2, 5, 0),
                  (".$idUsuario.", 3, 5, 0),
                  (".$idUsuario.", 4, 5, 0),
                  (".$idUsuario.", 5, 5, 0),
                  (".$idUsuario.", 6, 5, 0),
                  (".$idUsuario.", 1, 6, 0),
                  (".$idUsuario.", 2, 6, 0),
                  (".$idUsuario.", 3, 6, 0),
                  (".$idUsuario.", 4, 6, 0);";
        
        $result = mysql_query($sql2, $this->conexionDAO->Conexion_ID);
        
    }
    
    function isSussess($usuario,$clave){

        $sql = 'SELECT * from usuario where usuario = "'.mysql_real_escape_string($usuario).'"
            and clave="'.mysql_real_escape_string($clave).'"';
        
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        
        if($numregistros == 0){
            return 0;
        }
        return 1;
    }
    
    function getIdByUsername($user){

        $sql = 'SELECT * from usuario where usuario = "'.mysql_real_escape_string($user).'"';
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        if($numregistros == 0){
            return null;
        }
        return $this->conexionDAO->ObjetoConsulta2[0][0];
    }
    
    function getUserById($id){
        $usuario = new Usuario();
        $sql = 'SELECT * from usuario where id = "'.mysql_real_escape_string($id).'"';
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        if($numregistros == 0){
            return null;
        }

        $usuario->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $usuario->setNombres($this->conexionDAO->ObjetoConsulta2[0][1]);
        $usuario->setEmail($this->conexionDAO->ObjetoConsulta2[0][2]);
        $usuario->setUsuario($this->conexionDAO->ObjetoConsulta2[0][3]);
        $usuario->setClave($this->conexionDAO->ObjetoConsulta2[0][4]);
        $usuario->setIsAdmin($this->conexionDAO->ObjetoConsulta2[0][5]);

        return $usuario;
    }
    
    function getUserByName($user){
        $usuario = new Usuario();
        $sql = 'SELECT * from usuario where usuario = "'.mysql_real_escape_string($user).'"';
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        if($numregistros == 0){
            return null;
        }

        $usuario->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $usuario->setNombres($this->conexionDAO->ObjetoConsulta2[0][1]);
        $usuario->setEmail($this->conexionDAO->ObjetoConsulta2[0][2]);
        $usuario->setUsuario($this->conexionDAO->ObjetoConsulta2[0][3]);
        $usuario->setClave($this->conexionDAO->ObjetoConsulta2[0][4]);
        $usuario->setIsAdmin($this->conexionDAO->ObjetoConsulta2[0][5]);

        return $usuario;
    }
    
    function getUserByEmail($email){
        $usuario = new Usuario();
        $sql = 'SELECT * from usuario where email = "'.mysql_real_escape_string($email).'"';
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        if($numregistros == 0){
            return null;
        }

        $usuario->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $usuario->setNombres($this->conexionDAO->ObjetoConsulta2[0][1]);
        $usuario->setEmail($this->conexionDAO->ObjetoConsulta2[0][2]);
        $usuario->setUsuario($this->conexionDAO->ObjetoConsulta2[0][3]);
        $usuario->setClave($this->conexionDAO->ObjetoConsulta2[0][4]);
        $usuario->setIsAdmin($this->conexionDAO->ObjetoConsulta2[0][5]);

        return $usuario;
    }
    
}
?>
