<?php

class situacionDAO {

    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function save($situacion){
        $nuevaSituacion = $situacion;
        $querty =   "insert into situacion
                    (nombre, descripcion, id_usuario, enable, orden)
                    values(
                    \"".mysql_real_escape_string($nuevaSituacion->getNombre())."\",
                    \"".mysql_real_escape_string($nuevaSituacion->getDescripcion())."\",
                    \"".mysql_real_escape_string($nuevaSituacion->getIdUsuario())."\",
                    \"".mysql_real_escape_string($nuevaSituacion->getEnabled())."\",
                    \"".mysql_real_escape_string($nuevaSituacion->getOrden())."\"
                    )";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveSituacio): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function getSituacionesByUsuario($usuarioId){
        
        $sql="SELECT * FROM situacion WHERE id_usuario=".$usuarioId;
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $situacion = new Situacion(); 
            $situacion->setId($this->conexionDAO->ObjetoConsulta2[$i][0]);
            $situacion->setNombre($this->conexionDAO->ObjetoConsulta2[$i][1]);
            $situacion->setDescripcion($this->conexionDAO->ObjetoConsulta2[$i][2]);
            $situacion->setIdUsuario($this->conexionDAO->ObjetoConsulta2[$i][3]);
            $situacion->setEnabled($this->conexionDAO->ObjetoConsulta2[$i][4]);
            $situacion->setisEnded($this->conexionDAO->ObjetoConsulta2[$i][5]);
            $lista[$i] = $situacion;
        }
        
        return $lista;
        
    }
    
    function getSelectedSituacion($idUsuario){
        $sql="SELECT * FROM situacion WHERE id_usuario=".$idUsuario." AND enable=1";
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $situacion = new Situacion();
        if($numregistros == 0){
            return $situacion;
        }
        
        $situacion->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $situacion->setNombre($this->conexionDAO->ObjetoConsulta2[0][1]);
        $situacion->setDescripcion($this->conexionDAO->ObjetoConsulta2[0][2]);
        $situacion->setIdUsuario($this->conexionDAO->ObjetoConsulta2[0][3]);
        $situacion->setEnabled($this->conexionDAO->ObjetoConsulta2[0][4]);
        $situacion->setisEnded($this->conexionDAO->ObjetoConsulta2[0][5]);
        $situacion->setSucedio($this->conexionDAO->ObjetoConsulta2[0][6]);
        $situacion->setCambios($this->conexionDAO->ObjetoConsulta2[0][7]);
        $situacion->setConsecuencias($this->conexionDAO->ObjetoConsulta2[0][8]);
        $situacion->setOrden($this->conexionDAO->ObjetoConsulta2[0][9]);
        
        return $situacion; 
    }
    
    function setEnd($idUsuario){
        $querty =   "UPDATE situacion SET isEnded = 1 WHERE id_usuario = ".$idUsuario." AND enable=1";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	//echo $querty;	
        if (!$result){
            echo 'Ooops (updatetema): '.mysql_error();
            return false;
        }

        return true;
    }
    
    function isEnded($idUsuario){
        $sql = "SELECT * FROM situacion WHERE id_usuario=".$idUsuario." AND enable=1 AND isEnded=1";
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        
        if($numregistros == 0){
            return false;
        }
        return true;
    }
    function setDescripcion($idSituacion,$descripcion,$sucedio,$cambios,$consecuencias){
        $querty =   "UPDATE situacion SET
                    descripcion =
                    \"".mysql_real_escape_string($descripcion)."\",
                    sucedio =
                    \"".mysql_real_escape_string($sucedio)."\",
                    cambios =
                    \"".mysql_real_escape_string($cambios)."\",
                    consecuencias =
                    \"".mysql_real_escape_string($consecuencias)."\"
                    WHERE id =
                    ".mysql_real_escape_string($idSituacion)."
                    ";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	//echo $querty;	
        if (!$result){
            echo 'Ooops (updatetema): '.mysql_error();
            return false;
        }

        return true;
        
    }
    
    function reset($idUsuario){
        $querty =   "UPDATE situacion SET enable = 0, isEnded=0 WHERE id_usuario =".$idUsuario;
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	//echo $querty;	
        if (!$result){
            echo 'Ooops (resetSituacion): '.mysql_error();
            return false;
        }

        return true;
    }
    
    function update($situacion){
        
        $querty =   "UPDATE situacion SET 
                    nombre = '".$situacion->getNombre()."',
                    enable= ".$situacion->getEnabled().",
                    orden = '".$situacion->getOrden()."'
                    WHERE id=".$situacion->getId();
        
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (update Situacion): '.mysql_error();
            return false;
        }
        return true;
    }
}

?>
