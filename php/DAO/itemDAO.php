<?php

class itemDAO {

    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }
    
    function getItemsByFase($idFase){
        
        $sql="SELECT * FROM item WHERE id_fase IN (SELECT id_fase FROM usuario_item WHERE id_fase = ".$idFase.") order by numero";
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $item = new Item(); 
            $item->setId($this->conexionDAO->ObjetoConsulta2[$i][0]);
            $item->setNumero($this->conexionDAO->ObjetoConsulta2[$i][1]);
            $item->setTitulo($this->conexionDAO->ObjetoConsulta2[$i][2]);
            $item->setIdFase($this->conexionDAO->ObjetoConsulta2[$i][3]);
            $item->setTipo($this->conexionDAO->ObjetoConsulta2[$i][4]);
            $lista[$i] = $item;
        }
        
        return $lista;
        
    }
    
    function isEnabled($idUsuario,$idItem,$idFase){
        $sql="SELECT estado FROM usuario_item WHERE id_usuario='".$idUsuario."' AND id_fase='".$idFase."' AND id_item='".$idItem."'";
        
        $this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();

        if($numregistros == 0){
            return null;
        }

        return $this->conexionDAO->ObjetoConsulta2[0][0];
    }
   
    function setComplete($idFase, $idItem, $idUsuario){
        $sql="UPDATE usuario_item SET estado=1 WHERE id_usuario=".$idUsuario." AND id_fase=".$idFase." AND id_item=".$idItem;
        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        
	if (!$result){
            echo 'Ooops (updateItem): '.mysql_error();
            return false;
        }
        
        return true;
    }
    
    function getTipo($numero,$id_fase){
        $sql="SELECT tipo FROM item WHERE numero=".$numero." AND id_fase=".$id_fase;
        
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros(); 
        if($numregistros == 0){
            return $fase;
        }
        
        
        return $this->conexionDAO->ObjetoConsulta2[0][0];  
    }
}

?>
