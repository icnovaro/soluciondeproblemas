<?php

class historiaDAO {

    /****************************
    *         ATRIBUTOS         *
    ****************************/

    /**
     * conexionDAO: Mantiene el estado de conexión con la base de datos
     */
    public $conexionDAO;
    
    
    /****************************
    *       CONSTRUCTORES       *
    ****************************/

    /**
     * Constructor sin parámetros de la clase
     *  Establece conexión con la base de d谩tos
     */
    
    function __construct(){
    $this->conexionDAO = new conexion();
    $this->conexionDAO->conectar();
    }

  
    function getHistoriaByUsuario($usuarioId){
        
        $sql="SELECT * FROM historia WHERE id_usuario=".$usuarioId;
	$this->conexionDAO->consulta($sql);
        $this->conexionDAO->leerVarios();
        $numregistros = $this->conexionDAO->numregistros();
        $historia = new Historia(); 
        if($numregistros == 0){
            return $historia;
        }

        $historia->setId($this->conexionDAO->ObjetoConsulta2[0][0]);
        $historia->setIdUsuario($this->conexionDAO->ObjetoConsulta2[0][1]);
        $historia->setDesc($this->conexionDAO->ObjetoConsulta2[0][2]);
        $historia->setDesc1($this->conexionDAO->ObjetoConsulta2[0][3]);
        $historia->setDesc2($this->conexionDAO->ObjetoConsulta2[0][4]);
        $historia->setDesc3($this->conexionDAO->ObjetoConsulta2[0][5]);
        
        return $historia; 
        
    }
    
    
    function save($historia){
        $nuevaHistoria = $historia;
        $querty =   "insert into historia
                    (id_usuario, descn, desc1, desc2, desc3)
                    values(
                    \"".mysql_real_escape_string($nuevaHistoria->getIdUsuario())."\",
                    \"".mysql_real_escape_string($nuevaHistoria->getDesc())."\",
                    \"".mysql_real_escape_string($nuevaHistoria->getDesc1())."\",
                    \"".mysql_real_escape_string($nuevaHistoria->getDesc2())."\",
                    \"".mysql_real_escape_string($nuevaHistoria->getDesc3())."\"
                    )";
        $result = mysql_query($querty, $this->conexionDAO->Conexion_ID);
	
        if (!$result){
            //Mensaje de error
            echo 'Ooops (saveHistoria): '.mysql_error();
            return false;
        }
        return true;
    }
    
    function updateHistoria($historia){
        $sql="UPDATE historia SET desc1='".$historia->getDesc1()."',
            desc2='".$historia->getDesc2()."',
            desc3='".$historia->getDesc3()."' 
            WHERE id='".$historia->getId()."'";

        $result = mysql_query($sql, $this->conexionDAO->Conexion_ID);
        
	if (!$result){
            echo 'Ooops (updateHistoria): '.mysql_error();
            return false;
        }
        
        return true;
    }
}

?>
