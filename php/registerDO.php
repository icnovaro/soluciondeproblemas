<?php
include_once ('DAO/conectionDAO.php');
include_once ('DAO/codigoDAO.php');
include_once ('DAO/usuarioDAO.php');
include_once ('DAO/usuarioDAO.php');

$codigoDAO = new codigoDAO();

$nombres=$_POST['nombres'];
$usuario=$_POST['usuario'];
$clave1=$_POST['clave'];
$clave2=$_POST['clave2'];
$email=$_POST['email'];
$codigo=$_POST['codigo'];



//Verify errors
if($usuario == "" || $clave1 == "" || $clave2 == "" || $email== "" || $codigo == "" || $nombres == ""){
    header("location: ./../registro.php?error0");
    exit;
}
if($clave1!=$clave2){
    header("location: ./../registro.php?error1");
    exit;
}
if(strlen($clave1)<7){
    header("location: ./../registro.php?error2");
    exit;
}
if($codigoDAO->validateCodigo($codigo)==false){
    header("location: ./../registro.php?error3");
    exit;
}
$permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"; 

for ($i=0; $i<strlen($usuario); $i++){ 
   if (strpos($permitidos, substr($usuario,$i,1))===false){ 
        header("location: ./../registro.php?error6");
        exit;
   } 
}

$usuarioDAO= new UsuarioDAO();

if($usuarioDAO->userExists($usuario)==true){
    header("location: ./../registro.php?error4");
    exit;
}
if($usuarioDAO->emailExists($email)==true){
    header("location: ./../registro.php?error5");
    exit;
}

$usuarioDAO->save($nombres, $usuario, $clave1, $email, $codigo);
header("location: ./../registro.php?ok");
exit;

/*if($usuarioDAO->isSussess($usuario, $clave)==1){
    session_start();
    $_SESSION['user']=$usuarioDAO->getIdByUsername($usuario);
    header("location: ./../fases.php");
}else{
    header("location: ./../login.php?error1");
    exit;
}*/
?>
