<?php session_start();?>
<?php
if( !isset($_SESSION['user']) ){
    header("location: ./../index.php");
    exit;
}
    header("Content-type: text/html; charset=utf-8");
    
    include 'DAO/conectionDAO.php';
    include 'DAO/itemDAO.php';
    include 'DAO/faseDAO.php';
    include 'DAO/historiaDAO.php';
    include 'DAO/situacionDAO.php';
    include 'DAO/subproblemaDAO.php';
    include 'DAO/alternativaDAO.php';
    include 'Entities/Historia.php';
    include 'Entities/Item.php';
    include 'Entities/Situacion.php';
    include 'Entities/Subproblema.php';
    include 'Entities/Alternativa.php';
    
    $idFase=$_POST['idFase'];
    $idItem=$_POST['idItem'];
    $idUsuario=$_POST['idUsuario'];
    
    $url="location: ./../fase.php?id=".$idFase."&i=".$idItem;
    
    //Validación de tablas
    
    $itemDAO = new itemDAO();
    $historiaDAO = new historiaDAO();

    if($itemDAO->getTipo($idItem, $idFase)==1){
        //Validación de campos para la tabla de la fase 1
        if($idFase==1){
            $desc=$_POST['desc'];
            $desc1=$_POST['desc1'];
            $desc2=$_POST['desc2'];
            $desc3=$_POST['desc3'];
            
            
            if(strlen($desc)*strlen($desc1)*strlen($desc2)*strlen($desc3) > 0){
                $historia = new Historia();
                $historia->setIdUsuario($idUsuario);
                $historia->setDesc($desc);
                $historia->setDesc1($desc1);
                $historia->setDesc2($desc2);
                $historia->setDesc3($desc3);
                $historiaDAO->save($historia);
            }            
        }
        if($idFase==2){
            $situacionDAO= new situacionDAO();
            $situaciones = $situacionDAO->getSituacionesByUsuario($idUsuario);
            if($situaciones!=null && isset($situaciones)>0){
                $j=1;
                foreach($situaciones as $oldSituacion){
                    $situacion= new Situacion();
                    $situacion->setNombre($_POST['a'.$j]);
                    $situacion->setDescripcion("");
                    $situacion->setIdUsuario($idUsuario);
                    $situacion->setId($oldSituacion->getId());
                  
                    if($j==$_POST['seleccion0']){
                        $situacion->setEnabled(1);
                        $orden="";
                        for($i=0;$i<11;$i++){
                            $orden.=$_POST['seleccion'.$i]."-";
                        }
                        $situacion->setOrden($orden);
                    }else{
                        $situacion->setEnabled(0);
                        $situacion->setOrden("");
                    }

                    $situacionDAO->update($situacion);
                    $j++;
                }
            }else{
                for($j=1;$j<12;$j++){
                    $situacion= new Situacion();
                    $situacion->setNombre($_POST['a'.$j]);
                    $situacion->setDescripcion("");
                    $situacion->setIdUsuario($idUsuario);

                    if($j==$_POST['seleccion0']){
                        $situacion->setEnabled(1);
                        $orden="";
                        for($i=0;$i<11;$i++){
                            $orden.=$_POST['seleccion'.$i]."-";
                        }
                        $situacion->setOrden($orden);
                    }else{
                        $situacion->setEnabled(0);
                        $situacion->setOrden("");
                    }
                    $situacionDAO->save($situacion);
                }
            }
        }
        if($idFase==3){
            
            $situacionDAO= new situacionDAO();
            $situacion= $situacionDAO->getSelectedSituacion($idUsuario);
            $situacionDAO->setDescripcion($situacion->getId(), $_POST['desc'],$_POST['desc1'],$_POST['desc2'],$_POST['desc3']);
            

            if(strlen($_POST['desc1'])*strlen($_POST['desc2'])*strlen($_POST['desc3'])==0){
                header($url."&e1");
                exit;
            }
            /*$historia=$historiaDAO->getHistoriaByUsuario($idUsuario);
            
            $historia->setDesc1();
            $historia->setDesc2($_POST['desc2']);
            $historia->setDesc3($_POST['desc3']);
            
            $historiaDAO->updateHistoria($historia);*/
            
            $subproblemaDAO= new subproblemaDAO();
          
            //Si la situación ya se terminó entonces no se crean nuevos datos
            //se actualizan.
            if($situacionDAO->isEnded($idUsuario)){
                
                $subproblemas=$subproblemaDAO->getSubproblemasBySituacion($situacion->getId());
                $aux=1;
                foreach ($subproblemas as  $oldSubproblema){
                    
                    $subproblema= new Subproblema();
                    $subproblema->setId($oldSubproblema->getId());
                    $subproblema->setDescripcion($_POST['a'.$aux.'a']);
                    $subproblema->setObjetivo($_POST['a'.$aux.'b']);
                    $subproblema->setObstaculo($_POST['a'.$aux.'c']);
                    $subproblema->setIdSituacion($situacion->getId());
                    $subproblemaDAO->update($subproblema);
                    $aux++;
                }
            }else{
                for($k=1;$k<4;$k++){
                    $subproblema= new Subproblema();
                    $subproblema->setDescripcion($_POST['a'.$k.'a']);
                    $subproblema->setObjetivo($_POST['a'.$k.'b']);
                    $subproblema->setObstaculo($_POST['a'.$k.'c']);
                    $subproblema->setIdSituacion($situacion->getId());
                    $subproblemaDAO->save($subproblema);
                }
            }
        }
        if($idFase==4){ 
            $alternativaDAO = new alternativaDAO();
            
            $subproblemaDAO= new subproblemaDAO();
            $subproblemas= $subproblemaDAO->getSubproblemasBySituacion($_POST['situacion']);
            
            //Se eliminan las alternativas asociadas a un subproblema


            for($i=1; $i<4; $i++){
                //Crear array
                
                $aux=1;
                while(isset($_POST['hidden-'.$i.'-'.$aux])){
                    $alternativa= new Alternativa();
                    $sub=$subproblemas[$i-1];
                    $array=  explode ('-',$_POST['hidden-'.$i.'-'.$aux]);
                    $subProblema=$array[0];
                    $item=$array[1];
                    $check=$array[2];
                    $descripcion = $_POST['a'.$subProblema.'-alt'.$item];
                    $consecuencia = $_POST['a'.$subProblema.'-con'.$item];
                    
                    $alternativa->setConsecuencia($consecuencia);
                    $alternativa->setDescripcion($descripcion);
                    $alternativa->setValoracion($check);
                    $alternativa->setProbabilidad($check);
                    $alternativa->setIdSubproblema($sub->getId());
                    if(strlen(trim($descripcion))* strlen(trim($descripcion))==0){
                       header($url."&err1");
                       exit;
                    }
                    $alternativaDAO->delete($sub->getId());
                    $alternativaDAO->save($alternativa);
                    
                    $aux++;
                }
            }        
        }
        if($idFase==5){  
            $alternativaDAO = new alternativaDAO();
            $subproblemaDAO= new subproblemaDAO();
            $subproblemas= $subproblemaDAO->getSubproblemasBySituacion($_POST['situacion']);
            
            for($i=1; $i<4; $i++){
                //Crear array
                $autoregistro = $_POST['auto'.$i];
                $sub=$subproblemas[$i-1];
                $subproblemaDAO->updateAutoregistro($sub->getId(), $autoregistro);
                $aux=1;
                while(isset($_POST['hidden-'.$i.'-'.$aux])){
                    $alternativa= new Alternativa();
                    $array=  explode ('-',$_POST['hidden-'.$i.'-'.$aux]);
                    $subProblema=$array[0];
                    $item=$array[1];
                    $check=$array[2];
                    $descripcion = $_POST['a'.$subProblema.'-alt'.$item];
                    $consecuencia = $_POST['a'.$subProblema.'-con'.$item];
                    
                    $alternativa->setConsecuencia($consecuencia);
                    $alternativa->setDescripcion($descripcion);
                    $alternativa->setValoracion($check);
                    $alternativa->setProbabilidad($check);
                    $alternativa->setIdSubproblema($sub->getId());
                    if(strlen(trim($descripcion))* strlen(trim($descripcion))==0){
                       header($url."&err1");
                       exit;
                    }
                    $alternativaDAO->save($alternativa);
                    $aux++;
                }
            }     
        }
        if($idFase==6){ 
            $subproblemaDAO= new subproblemaDAO();
            $subproblemas= $subproblemaDAO->getSubproblemasBySituacion($_POST['situacion']);
            $aux=1;
            foreach($subproblemas as $subproblema){
                $aportes=$_POST['a'.$aux];
                $subproblemaDAO->updateAportes($subproblema->getId(), $aportes);
                $aux++;
            }
        }
    }
    $itemDAO->setComplete($idFase, $idItem, $idUsuario);
    
    
    $faseDAO = new faseDAO();
    if($faseDAO->isCompleted($idUsuario, $idFase)==true){
        $url.="&c";
    }
  
    header($url);

?>
