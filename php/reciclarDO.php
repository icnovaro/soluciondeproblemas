<?php
include_once ('DAO/conectionDAO.php');
include_once ('DAO/codigoDAO.php');
include_once ('DAO/usuarioDAO.php');

if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}
$idUsuario= $_SESSION['user'];
$usuarioDAO= new UsuarioDAO();

$usuarioDAO->reciclarProceso($idUsuario);

header("location: ./../fase.php?id=3");
exit;

?>
