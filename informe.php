<?php
    
    /**
     * HTML2PDF Librairy - example
     *
     * HTML => PDF convertor
     * distributed under the LGPL License
     *
     * @author      Laurent MINGUET <webmaster@html2pdf.fr>
     *
     * isset($_GET['vuehtml']) is not mandatory
     * it allow to display the result in the HTML format
     */
     
    include_once (dirname(__FILE__).'/includes/html2/html2pdf.class.php');
    
    include_once './php/Entities/Historia.php';
    include_once './php/Entities/Fase.php';
    include_once './php/Entities/Situacion.php';
    include_once './php/Entities/Subproblema.php';
    include_once './php/Entities/Alternativa.php';
    include_once './php/DAO/historiaDAO.php';
    include_once './php/DAO/faseDAO.php';
    include_once './php/DAO/situacionDAO.php';
    include_once './php/DAO/subproblemaDAO.php';
    include_once './php/DAO/alternativaDAO.php';
    include_once './php/DAO/conectionDAO.php';
    
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

    if( !isset($_SESSION['user']) ){
        header("location: ./index.php");
        exit;
    } 
    
    
    function getProbabilidad($val){
        
        switch ($val){
            case "1":
                return "Poco Probable";
                break;
            case "2":
                return "Probable";
                break;
            case "3":
                return "Muy Probable";
                break;
        }
        return "0";
    }
    
    function getValoracion($val){
        
        switch ($val){
            case "1":
                return "Baja";
                break;
            case "2":
                return "Media";
                break;
            case "3":
                return "Alta";
                break;
        }
        return "0";
    }
    
    $idUsuario= $_SESSION['user'];
    //All DAO objects
    
    $historiaDAO= new historiaDAO();

    $faseDAO= new faseDAO();
    $situacionDAO = new situacionDAO();
    
    $historia=$historiaDAO->getHistoriaByUsuario($idUsuario);
    
    
    // get the HTML
    ob_start();
    
    $content='<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">';
    $content.=' <page_header>';
    $content.='     <table style="width: 100%; border: solid 1px black;">';
    $content.='         <tr>';
    $content.='             <td style="text-align: left;    width: 33%">Informe</td>';
    $content.='             <td style="text-align: center;  width: 34%">Solución de Problemas Online</td>';
    $content.='             <td style="text-align: right;    width: 33%">'.date('d/m/Y').'</td>';     
    $content.='         </tr>';
    $content.='     </table>';        
    $content.=' </page_header>';
    $content.='<page_footer>';
    $content.=' <table style="width: 100%; border: solid 1px black;">';
    $content.='     <tr>';
    $content.='         <td style="text-align: left;    width: 50%">Informe</td>';
    $content.='         <td style="text-align: right;    width: 50%">page [[page_cu]]/[[page_nb]]</td>';
    $content.='     </tr>';
    $content.=' </table>';    
    $content.='</page_footer>';    
    $content.='<span style="font-size: 22px; font-weight: bold">Informe de solución del problema</span><br><br><br>';
    
    //FASE 1
    if($faseDAO->getLastFase($idUsuario)==null || $faseDAO->getLastFase($idUsuario)->getNumero()>1)
        {
        $content.='<span style="font-size: 16px; font-weight: bold">1.Experiencia en solución de problemas</span><br>';
        $content.='<br><br>';
        $content.='<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="left">';     
        $content.=' <tbody>';            
        $content.=' <tr>';
        $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" >';
        $content.='         Descripción del problema que usted había solucionado desde su experiencia religiosa.';
        $content.='     </td>';
        $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.=$historia->getDesc();
        $content.='     </td>';
        $content.=' </tr>';
        $content.=' <tr>';
        $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.='         ¿Qué sucedió antes del problema?';
        $content.='     </td>';
        $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.=$historia->getDesc1();
        $content.='     </td>';
        $content.=' </tr>';
        $content.=' <tr>';
        $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.='         ¿Qué cambios realizó para solucionar el problema?';
        $content.='     </td>';
        $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.=$historia->getDesc2();
        $content.='     </td>';
        $content.=' </tr>';
        $content.=' <tr>';
        $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.='         ¿Cuáles fueron las consecuencias después de solucionar el problema?';
        $content.='     </td>';
        $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
        $content.=$historia->getDesc3();
        $content.='     </td>';
        $content.=' </tr>';

        $content.=' </tbody>';
        $content.='</table>';  
        $content.='<br>';
    }
    //FINAL DE LA FASE 1
    $content.='<span style="font-size: 16px; font-weight: bold">2.Resumen de la solución del problema</span><br><br><br>';
    $content.='<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="left">';
    $content.=' <tbody>'; 
    //FASE 2
    if($faseDAO->getLastFase($idUsuario)==null || $faseDAO->getLastFase($idUsuario)->getNumero()>2)
    {
        $situacion= $situacionDAO->getSelectedSituacion($idUsuario);
        
        $content.='<tr>';
        $content.=' <th style="width: 30%; text-align: left; border: solid 1px #337722; background: #CCFFCC" colspan="4">Situación problemática</th>';
        $content.='</tr>';  
	$content.="<tr>";
        $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" colspan="2"><b>Nombre de la situación elegida</b></td>';
        $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" colspan="2">'.$situacion->getNombre().'</td>';
	$content.="</tr>"; 
    }
    //FINAL FASE 2
    $idSituacion=0;
    $subproblemaDAO = new subproblemaDAO();
    //FASE 3
    if($faseDAO->getLastFase($idUsuario)==null || $faseDAO->getLastFase($idUsuario)->getNumero()>3)
    {
        $situacion= $situacionDAO->getSelectedSituacion($idUsuario);
        $idSituacion=$situacion->getId();
        
        $content.="<tr>";
        $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" colspan="2"><b>Descripción de la situación elegida</b></td>';
        $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" colspan="2">'.$situacion->getDescripcion().'</td>';
	$content.="</tr>";
        
        $subproblemas= $subproblemaDAO->getSubproblemasBySituacion($idSituacion);
        
        $content.="<tr>";
        $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Subproblemas</td>';
        $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Descripción</td>';
        $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Objetivo</td>';
        $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Obstáculos que se me pueden presentar para lograr mi objetivo</td>';
	$content.="</tr>";
        $aux=1;
        foreach($subproblemas as $item){
            $content.=' <tr>';
            $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">Subproblema '.$aux.'</td>';
            $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.$item->getDescripcion().'</td>';
            $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.$item->getObjetivo().'</td>';
            $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.$item->getObstaculo().'</td>';
            $content.=' </tr>';
            $aux++;
        }
    }
    $content.=' </tbody>';
    $content.='</table>';
    $content.='<br><br>';
    //FINAL FASE 3
    $content.='<span style="font-size: 16px; font-weight: bold">2.1 Resumen de alternativas para los sub-problemas</span><br><br><br>';
    
    $alternativaDAO= new alternativaDAO();
    //FASE4
    $content.='<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="left">';
    $content.=' <tbody>'; 
    if($faseDAO->getLastFase($idUsuario)==null || $faseDAO->getLastFase($idUsuario)->getNumero()>4)
    {
        $subproblemas= $subproblemaDAO->getSubproblemasBySituacion($idSituacion);
        $aux=1;
        foreach($subproblemas as $item){

            if($faseDAO->getLastFase($idUsuario)==null || $faseDAO->getLastFase($idUsuario)->getNumero()>5){
                $content.="<tr>";
                $content.=' <td style="width: 50%; text-align: left; border: solid 1px #337722; background: #CCFFCC" colspan="2">Subproblema '.$aux.'</td>';
                $content.=' <td style="width: 50%; text-align: left; border: solid 1px #337722; background: #CCFFCC" colspan="2">Autoregistro de la implementación</td>';
                $content.="</tr>";
                $content.="<tr>";
                $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" colspan="2">'.$item->getDescripcion().'</td>';
                $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" colspan="2">'.$item->getAutoregistro().'</td>';
                $content.="</tr>";
            
            }else{
                $content.="<tr>";
                $content.=' <td style="width: 100%; text-align: left; border: solid 1px #337722; background: #CCFFCC" colspan="4">Subproblema '.$aux.'</td>';
                $content.="</tr>";
            }
            $content.="<tr>";
            $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Alternativa</td>';
            $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Consecuencia</td>';
            $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Valoración</td>';
            $content.=' <td style="width: 25%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Probabilidad</td>';
            $content.="</tr>";
            
            $alternativas = $alternativaDAO->getAlternativasBySubproblema($item->getId());
            
            foreach ($alternativas as $alt){
                $content.=' <tr>';
                $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.$alt->getDescripcion().'</td>';
                $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.$alt->getConsecuencia().'</td>';
                $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.getValoracion($alt->getValoracion()).'</td>';
                $content.='     <td style="width: 25%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.getProbabilidad($alt->getProbabilidad()).'</td>';
                $content.=' </tr>';
                 
            }
          $aux++;   
        }
    }
    
    $content.=' </tbody>';
    $content.='</table>';
    $content.='<br><br>';
    //FINAL FASE 4
    //FINAL FASE 6
    $content.='<span style="font-size: 16px; font-weight: bold">2.2 Aportes de la experiencia religiosa</span><br><br><br>';
    $content.='<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="left">';
    $content.=' <tbody>'; 
    if($faseDAO->getLastFase($idUsuario)==null)
    {
        $content.="<tr>";
            $content.=' <td style="width: 50%; text-align: left; border: solid 1px #337722; background: #CCFFCC" >Subproblemas</td>';
            $content.=' <td style="width: 50%; text-align: left; border: solid 1px #337722; background: #CCFFCC" >Aportes</td>';
        $content.="</tr>";

        $subproblemas= $subproblemaDAO->getSubproblemasBySituacion($idSituacion);
        $aux=1;
        foreach($subproblemas as $item){
            $content.="<tr>";
                $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">Subproblema '.$aux.'</td>';
                $content.=' <td style="width: 50%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">'.$item->getAportes().'</td>';
            $content.="</tr>";
            $aux++;
        }
    }
    $content.=' </tbody>';
    $content.='</table>';



    $content.='</page>';
    //$content.='<page pageset="old">';
    //$content.=' Nouvelle page !!!!';
    //$content.='</page>';
    
    
    // convert to PDF
    
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 3);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple03.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
    ?>
