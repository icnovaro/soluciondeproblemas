<?php session_start();?>
<?php
if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}

include '../php/DAO/conectionDAO.php';
include '../php/Entities/Respuesta.php';
include '../php/DAO/respuestaDAO.php';

$idPregunta=$_POST['p'];
$idEncuesta=$_POST['e'];
$puntaje=$_POST['v'];

$respuesta = new Respuesta();

$respuesta->setIdPregunta($idPregunta);
$respuesta->setUsuarioEncuesta($idEncuesta);
$respuesta->setPuntaje($puntaje);


$respuestaDAO=new respuestaDAO();

$arr;

try{
    
    $respuestaItem=$respuestaDAO->getRespuesta($idPregunta, $idEncuesta);
    
    if($respuestaItem==null){
        
        $respuestaDAO->save($respuesta);
        $arr = array('message' => 'La nota ha sido guardada correctamente');
        
    }else{
        //$respuestaItem->setPuntaje($puntaje);
        //$respuestaDAO->update($respuestaItem);
        $arr = array('message' => 'La nota ha sido editada correctamente');
    }
}catch(Exception $e){
    $arr = array('message' => $e->getMessage());
}

echo json_encode($arr);

?>