<?php 


$idUsuario= $_SESSION['user'];

$situacionDAO = new situacionDAO();
$situacion= $situacionDAO->getSelectedSituacion($idUsuario);

$historiaDAO = new historiaDAO();
$historia= $historiaDAO->getHistoriaByUsuario($idUsuario);

$desc= $a1a=$a1b=$a1c=$a2a=$a2b=$a2c = $a3a = $a3b = $a3c= "";

$desc1= $historia->getDesc1();
$desc2= $historia->getDesc2();
$desc3= $historia->getDesc3();

$subproblemaDAO= new subproblemaDAO();
$subproblemas= $subproblemaDAO->getSubproblemasBySituacion($situacion->getId());

if(sizeof($subproblemas)>0){
    $a1a = $subproblemas[0]->getDescripcion();
    $a1b = $subproblemas[0]->getObjetivo();
    $a1c = $subproblemas[0]->getObstaculo();
    
    $a2a = $subproblemas[1]->getDescripcion();
    $a2b = $subproblemas[1]->getObjetivo();
    $a2c = $subproblemas[1]->getObstaculo();
    
    $a3a = $subproblemas[2]->getDescripcion();
    $a3b = $subproblemas[2]->getObjetivo();
    $a3c = $subproblemas[2]->getObstaculo();
}

if(isset($_GET['e1'])){
    echo '<p class="message-box-error"><strong>Error</strong> - No deben haber cuadros en blanco</p>';
}
?>



<h2>Formulación del problema</h2>

<p><b>Problema seleccionado:</b> <?php echo $situacion->getNombre(); ?> </p>
<ul>
    <li>
       <p>Describir la situación problemática :</p>
       <textarea rows="2" cols="80" name="desc" required><?php echo $situacion->getDescripcion(); ?></textarea>
    </li>
</ul>
<table>
    <tr>
        <td><p>¿Qué sucedió antes del problema?</p></td>
        <td><p>¿Qué cambios realizó para solucionar el problema?</p></td>
        <td><p>¿Cuáles son las consecuencias negativas de este problema?</p></td>
    </tr>
    <tr>
        <td><textarea rows="7" cols="23" name="desc1" ><?php echo $situacion->getSucedio();?></textarea></td>
        <td><textarea rows="7" cols="23" name="desc2" ><?php echo $situacion->getCambios();;?></textarea></td>
        <td><textarea rows="7" cols="23" name="desc3" ><?php echo $situacion->getConsecuencias();?></textarea></td>
    </tr> 
</table>
<table border="1">
    <tr>
        <td></td>
        <td>Descripción del subproblema</td>
        <td>Objetivo específico del subproblema</td>
        <td>Obstáculos que se me pueden presentar para lograr mi objetivo.</td>
    </tr>
    <tr>
        <td>Sub-Problema 1</td>
        <td><textarea rows="7" cols="20" name="a1a" required><?php echo $a1a;?></textarea></td>
        <td><textarea rows="7" cols="20" name="a1b" required><?php echo $a1b;?></textarea></td>
        <td><textarea rows="7" cols="20" name="a1c" required><?php echo $a1c;?></textarea></td>
    </tr>
    <tr>
        <td>Sub-Problema 2</td>
        <td><textarea rows="7" cols="20" name="a2a" required><?php echo $a2a;?></textarea></td>
        <td><textarea rows="7" cols="20" name="a2b" required><?php echo $a2b;?></textarea></td>
        <td><textarea rows="7" cols="20" name="a2c" required><?php echo $a2c;?></textarea></td>
    </tr>
    <tr>
        <td>Sub-Problema 3</td>
        <td><textarea rows="7" cols="20" name="a3a" required><?php echo $a3a;?></textarea></td>
        <td><textarea rows="7" cols="20" name="a3b" required><?php echo $a3b;?></textarea></td>
        <td><textarea rows="7" cols="20" name="a3c" required><?php echo $a3c;?></textarea></td>
    </tr>
</table>

<br>

