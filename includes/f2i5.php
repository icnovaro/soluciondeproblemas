<?php 

header("Content-type: text/html; charset=utf-8");

$idUsuario= $_SESSION['user'];

$situacionDAO = new situacionDAO();
$historia= $situacionDAO->getSituacionesByUsuario($idUsuario);

$uno= $dos=$tres=$cuatro=$cinco=$seis=$siete = $ocho = $nueve = $diez = $once = "";

if(sizeof($historia)>0){
    $uno= $historia[0]->getNombre();
    $dos=$historia[1]->getNombre();
    $tres=$historia[2]->getNombre();
    $cuatro=$historia[3]->getNombre();
    $cinco=$historia[4]->getNombre();
    $seis = $historia[5]->getNombre();
    $siete = $historia[6]->getNombre();
    $ocho = $historia[7]->getNombre();
    $nueve = $historia[8]->getNombre();
    $diez = $historia[9]->getNombre();
    $once = $historia[10]->getNombre();
}

?>
<script type="text/javascript">
    var lista = ["",1,2,3,4,5,6,7,8,9,10,11];
    var lastId=1;
    var usadas=new  Array();
    
    $(document).ready(function() {
      llenarCajas();
      //seleccionar por defecto uno en el principal
      
      var control=document.getElementById('seleccion0');
      for(var i, k = 0; i = control.options[k]; k++) {
            if(i.value == 1) {
                control.selectedIndex = k;
                break;
            }
        }
    });
    
    function llenarCajas(){
        for(var j=0; j<11;j++){
            var sel = document.getElementById('seleccion'+j);
            for(var i = 0; i < lista.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = lista[i];
                opt.value = lista[i];
                sel.appendChild(opt);
            }
        }
    }
    
    function cuandoCambia(e){
        //2. limpiar array de usados
        usadas=new Array();
        var control=document.getElementById(e.name);
        if(e.name=="seleccion0" && e.value.length==0){
            alert("El número del problema principal no debe estar vacio");
            control.selectedIndex=lastId;
        }else{
            //3. recorrer todos los controles 
            for(var i=0; i<11;i++){
                //3.1 Recorrer diferentes al actual
                var nombre="seleccion"+i;
                var sel = document.getElementById(nombre);
                if(nombre!=e.name){
                    //4. para cada control ver que su valor es difente a ""
                    if(sel.value.length>0){
                        //alert(sel.name+" : "+sel.value);
                        //4.1 Si si es diferente agregar en el array de usados
                        usadas.push(sel.value+"-"+nombre);
                    }
                }
            }

            //5. Verificar si el valor actual está en el array de usados
            for(var j=0;j<usadas.length;j++){
                //5.1 si, el valor está mostrar un mensaje y borrar la selección
                var comp=usadas[j].split('-');
                if(e.value==comp[0]){
                    alert("El valor ya está en uso");
                    for(var i, k = 0; i = control.options[k]; k++) {
                        if(i.value == "") {
                            control.selectedIndex = k;
                            break;
                        }
                    }
                    return 0;
                }
            }
            
            if(e.name=="seleccion0"){
                lastId=e.value;
            }
        }
        
    }
   

</script>
<h2>Reconocimiento y Jerarquización de Situaciones Problemáticas</h2>

<ul>
    <li>
        <p>1. Describir las situaciones que me son molestas:</p>
        <textarea rows="2" cols="80" name="a1" required><?php echo $uno;?></textarea>
    </li>
    <li>
        <p>2. Describir las situaciones que evito:</p>
        <textarea rows="2" cols="80" name="a2" required><?php echo $dos;?></textarea>
    </li>
    <li>
        <p>3. Describir las situaciones en las que reacciono enfadado(a):</p>
        <textarea rows="2" cols="80" name="a3" required><?php echo $tres;?></textarea>
    </li>
    <li>
        <p>4. Describir las situaciones en las cuales me siento inseguro(a):</p>
        <textarea rows="2" cols="80" name="a4" required><?php echo $cuatro;?></textarea>
    </li>
    <li>
        <p>5. Describir las situaciones en las que experimento miedo :</p>
        <textarea rows="2" cols="80" name="a5" required><?php echo $cinco;?></textarea>
    </li>
    <li>
        <p>6. Describir las situaciones en las que no sé cómo comportarme :</p>
        <textarea rows="2" cols="80" name="a6" required><?php echo $seis;?></textarea>
    </li>
    <li>
        <p>7. Describir las situaciones en las que o he podido tomar decisiones :</p>
        <textarea rows="2" cols="80" name="a7" required><?php echo $siete;?></textarea>
    </li>
    <li>
        <p>8. Describir las situaciones en las que me siento insatisfecho :</p>
        <textarea rows="2" cols="80" name="a8" required><?php echo $ocho;?></textarea>
    </li>
    <li>
        <p>9. Describir las situaciones con las cuales no me siento bien:</p>
        <textarea rows="2" cols="80" name="a9" required><?php echo $nueve;?></textarea>
    </li>
    <li>
        <p>10. Describir las personas ante las cuales me siento inhibido :</p>
        <textarea rows="2" cols="80" name="a10" required><?php echo $diez;?></textarea>
    </li>
    <li>
        <p>11. Describir las personas de las cuales experimento dependencia:</p>
        <textarea rows="2" cols="80" name="a11" required><?php echo $once;?></textarea>
    </li>
    <li>
        <p>De los anteriores problemas seleccione el que es causa de algunos de los otros 
            problemas identificados el cuál será abordado en la terapia de solución de
            problemas a partir de la siguiente fase.</p>
    </li>
    <?php 
    $situacion= $situacionDAO->getSelectedSituacion($idUsuario);
    if(strlen($situacion->getOrden())>1){
        echo "<li>";
        echo "<div class='grafico'>";
        $array=explode("-", $situacion->getOrden());
        for($j=0;$j<11;$j++){
            echo '<div class="num'.($j+1).'">';
            echo '<select size=1 onchange="cuandoCambia(this);">';
                echo '<option disabled="disabled" value="">'.$array[$j].'</option>';
            echo '</select>';
            echo '</div>';
        }
        echo "</div></li>";
    }
    else{
    ?>
    <li>
        <div class="grafico">
            <div class="num1">
                <select name="seleccion0" id="seleccion0" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num2">
                <select name="seleccion1" id="seleccion1" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num3">
                <select name="seleccion2" id="seleccion2" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num4">
                <select name="seleccion3" id="seleccion3" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num5">
                <select name="seleccion4" id="seleccion4" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num6">
                <select name="seleccion5" id="seleccion5" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num7">
                <select name="seleccion6" id="seleccion6" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num8">
                <select name="seleccion7" id="seleccion7" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num9">
                <select name="seleccion8" id="seleccion8" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num10">
                <select name="seleccion9" id="seleccion9" size=1 onchange="cuandoCambia(this);"></select>
            </div>
            <div class="num11">
                <select name="seleccion10" id="seleccion10" size=1  onchange="cuandoCambia(this);"></select>
            </div>
       </div>
    </li>
    <?php 
    }
    ?>
</ul>
<br>

