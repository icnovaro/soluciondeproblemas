<?php 

$idUsuario= $_SESSION['user'];

$situacionDAO = new situacionDAO();
$subproblemaDAO = new subproblemaDAO();

$situacion= $situacionDAO->getSelectedSituacion($idUsuario);

$subproblemas= $subproblemaDAO->getSubproblemasBySituacion($situacion->getId());
?>

<h2>Auto Evaluación de la Implementación de Alternativas</h2>
<p>Evaluación de la puesta en práctica de las soluciones de cada sub-problema a partir del autoregistro: identificar los aportes de
la propia experiencia religiosa a la solución de problemas.</p>

<table>
    <?php
    $aux=1;
    foreach($subproblemas as $subproblema){
        echo "<tr>";
        echo "<td><b>Autoregistro Subproblema ".$aux."</b>:".$subproblema->getDescripcion()."</td>";
        echo "<td><b>Auto Evaluación: </b>".$subproblema->getAutoregistro()."</td>";
        echo "</tr><tr>";
        echo "<td>Auto Evaluación de la Solución del Subproblema ".$aux."</td>";
        echo "<td><textarea rows='2' cols='40' name='a".$aux."' required>".$subproblema->getAportes()."</textarea></td>";

        echo "</tr>";
        $aux++;
    }
    echo "<input type='hidden' name='situacion' value='".$situacion->getId()."'/>";
    ?>  
    
    
</table>
<br>

