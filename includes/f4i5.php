<?php 


$idUsuario= $_SESSION['user'];

$situacionDAO = new situacionDAO();
$situacion= $situacionDAO->getSelectedSituacion($idUsuario);

$subproblemaDAO= new subproblemaDAO();
$subproblemas= $subproblemaDAO->getSubproblemasBySituacion($situacion->getId());

$alternativaDAO = new alternativaDAO();
?>

<script type="text/javascript">
    function addElemento(e){
        var nombre=e.name;
        var nombreBoton = "."+e.name+"tr";
        var nombreTabla= "."+e.name+"tab";

        var $oldTab= $(document).find(nombreTabla);
        var $boton= $(document).find(nombreBoton);
        var $sep= $(document).find("."+e.name+"sep");

        var num = $(nombreTabla+" tr").length -4;
        var disable='disabled';
        var disableNum=$(nombreTabla+" tr").length-5;
        
        
        if(disableNum>=1){
            disable="";
        }
        
        
        $sep.remove();
        $boton.remove();
        $oldTab.append('<tr class="'+nombre+'-d'+num+'"><td><textarea rows="3" cols="18" name="'+nombre+'-alt'+num+'" class="'+nombre+'-alt'+num+'" onchange="cuandoCambia(this);" required></textarea></td><td><textarea rows="3" cols="18" name="'+nombre+'-con'+num+'" class="'+nombre+'-con'+num+'" required></textarea></td><td><input type="radio" name="'+nombre+'-val-'+num+'" value="1"  onchange="cuandoCambia(this);">-<br><input type="radio" name="'+nombre+'-val-'+num+'" value="2" onchange="cuandoCambia(this);">+<br><input type="radio" name="'+nombre+'-val-'+num+'" value="3"  onchange="cuandoCambia(this);" checked>++ </td><td><input type="radio" name="'+nombre+'-pro-'+num+'" value="1"  onchange="cuandoCambia(this);">PP<br><input type="radio" name="'+nombre+'-pro-'+num+'" value="2"  onchange="cuandoCambia(this);">P<br><input type="radio" name="'+nombre+'-pro-'+num+'" value="3"  onchange="cuandoCambia(this);" checked>MP</td></tr><tr class="'+nombre+'tr"><td><input type="button" value="Añadir alternativa" name="'+nombre+'" onclick="addElemento(this)"/></td><td><input type="button" value="Eliminar alternativa" name="'+nombre+'-d" onclick="removeElemento(this)" '+disable+'/></td><td></td><td></td></tr><tr class="'+nombre+'sep"><td colspan="4"><div class="separator"></div></td></tr>');
        llenarHTML();
    }
    
    function removeElemento(e){
        //hacer split del nombre
        var str=e.name;
        var nombre=str.split("-");
        var nombreTabla= "."+nombre[0]+"tab";
        
        var $oldTab= $(document).find(nombreTabla);
        var disableNum=$(nombreTabla+" tr").length-5;
        
        
        var $tr= $(document).find("."+nombre[0]+"-d"+disableNum);
        if(disableNum>1){
            $tr.remove();
            llenarHTML();
        }else{
            alert("Debe haber al menos una alternativa");
        }
    }
    
    function cuandoCambia(e){
        if(e.type=="radio"){
            var nombre=e.name.split('-');
            var value=$('input:radio[name='+e.name+']:checked').val();  
            if(nombre[1]=="val"){
                $('input:radio[name='+nombre[0]+'-pro-'+nombre[2]+']')[value-1].checked = true;
            }else{
                
                $('input:radio[name='+nombre[0]+'-val-'+nombre[2]+']')[value-1].checked = true;
            }
        }
        llenarHTML();
    }
    
    function llenarHTML(){
        var $resumen= $(document).find(".resumen");
        $resumen.empty();
        var text="<tr><td>";
        for(var i=1;i<4;i++){
            text+="<ul><li><b>Sub-Problema "+i+"</b></li>";
            //var count= $(".a"+i+"tab tr").length-4;
            var ordenados=getValues(i);
            for(var j=1 ;j<ordenados.length+1 && j<4; j++){
                text+="<li><input type='hidden' value='"+ordenados[j-1][2]+"-"+ordenados[j-1][0]+"' name='hidden-"+i+"-"+j+"'/>"+ordenados[j-1][1]+"</li>";
            }
            text+="</ul>";
        }
        text+="</td></tr>";
        $resumen.append(text);
    }
    
    function getValues(subproblema){
        var cantidadTr= $('.a'+subproblema+'tab tr').length-5;
        
        //1. traer todos los valores de un subproblema
        // y almacenarlos en una matriz [prioridad][texto]
        var prioridad=new Array();
        
        for(var i=0; i<cantidadTr; i++){
            prioridad[i]=new Array();
            prioridad[i][0]=parseInt($('input:radio[name=a'+subproblema+'-val-'+(i+1)+']:checked').val());
            prioridad[i][1]=$(".a"+subproblema+"-alt"+(i+1)).val();
            prioridad[i][2]=subproblema+"-"+(i+1);
        }
        //2. ordenar los valores de acuerdo a la prioridad
        prioridad.sort(function(a,b){
            return b[0] - a[0];
        });
        
        return prioridad
    }
    
</script>
<?php 
if(isset($_GET['err1'])){
    echo '<p class="message-box-error"><strong>Error</strong> - No deben haber cuadros en blanco</p>';
}
?>
<h2>Elaboración del alternativas</h2>

<?php 
     $itemDAO= new itemDAO();
     $habilitado= $itemDAO->isEnabled( $idUsuario, 5,4);
     if($habilitado=="1"){
         echo "<table class='resumen'>";
         //traer la situacion elegida
         $situacionDAO = new situacionDAO();
         $situacion= $situacionDAO->getSelectedSituacion($idUsuario);
         echo "<tr>";
         echo "<td colspan='2'><h3><b>Problema identificado:</b></h3><p>".$situacion->getDescripcion()."</p></td>";
         echo "</tr>";
         //traer los subproblemas asociados a la situacion
         $subproblemaDAO= new subproblemaDAO();
         $subproblemas=$subproblemaDAO->getSubproblemasBySituacion($situacion->getId());
         //recorrer los subproblemas
         $aux=1;
         foreach($subproblemas  as $subproblema){
             //traer las alternativas de cada subproblema
             echo "<tr>";
             echo "<td colspan='2'><h3><b>Subproblema ".$aux."</b></h3></td>";
             echo "</tr>";
             echo "<tr>";
             echo "<td><b>Descripcion</b></td>";
             echo "<td><b>Consecuencia</b></td>";
             echo "</tr>";
             $alternativaDAO = new alternativaDAO();
             $alternativas = $alternativaDAO->getAlternativasBySubproblema($subproblema->getId());
             foreach($alternativas as $alternativa){
                echo "<tr>";
                echo "<td>".$alternativa->getDescripcion()."</td>";
                echo "<td>".$alternativa->getConsecuencia()."</td>";
                echo "</tr>";
             }
             $aux++;
         }

         echo "</table>";
     }
     else{
?>

<p><b>Lluvia de Ideas y Toma de Decisiones:</b></p>
<ul>
    <li>
       <p><b>Convenciones para evaluar las alternativas seleccionadas :</b></p>
       <p><b>Valoración:</b> (-) negativa; (+) positiva; (++) Muy positiva.</p>
       <p><b>Probabilidad de ocurrencia:</b> (PP) Poco probable; (P) Probable; (MP)Muy probable..</p>
    </li>
</ul>

<table border="1" class="a1tab">

    <tr >
        <td colspan="4"><h3>Subproblema 1</h3></td>
    </tr>
    <tr>
        <td><b>Descripción: <?php echo $subproblemas[0]->getDescripcion();?></b></td>
        <td colspan="3"><b>Objetivos: <?php echo $subproblemas[0]->getObjetivo();?></b></td>
    </tr>
    <tr>
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    
        <?php 
            $alternativas = $alternativaDAO->getAlternativasBySubproblema($subproblemas[0]->getId());
            
            if(sizeof($alternativas)>0){
                $aux=1;
                foreach($alternativas as $alt){
                echo '<tr class="a1-d'.$aux.'">';
                echo '<td><textarea rows="3" cols="18" name="a1-alt'.$aux.'" class="a1-alt'.$aux.'" onchange="cuandoCambia(this);" required>'.$alt->getDescripcion().'</textarea></td>';
                echo '<td><textarea rows="3" cols="18" name="a1-con'.$aux.'" class="a1-alt'.$aux.'" required>'.$alt->getConsecuencia().'</textarea></td>';
                echo '<td>';
                for($i=1 ; $i<4 ; $i++){
                    $str="";
                    $symbol="";
                    switch ($i){
                        case 1:
                            $symbol="-";
                            break;
                        case 2:
                            $symbol="+";
                            break;
                        case 3:
                            $symbol="++";
                            break;
                    }
                    if($alt->getValoracion()==$i)
                        $str="checked";
                    echo '<input type="radio" name="a1-val-'.$aux.'" value="'.$i.'" onchange="cuandoCambia(this);" '.$str.'>'.$symbol.'<br>';
                }
                echo '</td><td>';
                for($i=1 ; $i<4 ; $i++){
                    $str="";
                    $symbol="";
                    switch ($i){
                        case 1:
                            $symbol="PP";
                            break;
                        case 2:
                            $symbol="P";
                            break;
                        case 3:
                            $symbol="MP";
                            break;
                    }
                    if($alt->getValoracion()==$i)
                        $str="checked";
                    echo '<input type="radio" name="a1-pro-'.$aux.'" value="'.$i.'" onchange="cuandoCambia(this);" '.$str.'>'.$symbol.'<br>';
                }
                echo '</td></tr>'; 
                $aux++;
                }
            }else{
        ?>
    <tr>
        <td><textarea rows="3" cols="18" name="a1-alt1" class="a1-alt1" onchange="cuandoCambia(this);" required></textarea></td>
        <td><textarea rows="3" cols="18" name="a1-con1" class="a1-alt1" required></textarea></td>
        <td>
            <input type="radio" name="a1-val-1" value="1" onchange="cuandoCambia(this);">-<br>
            <input type="radio" name="a1-val-1" value="2" onchange="cuandoCambia(this);">+<br>
            <input type="radio" name="a1-val-1" value="3" onchange="cuandoCambia(this);" checked>++ 
        </td>
        <td>
            <input type="radio" name="a1-pro-1" value="1" onchange="cuandoCambia(this);">PP<br>
            <input type="radio" name="a1-pro-1" value="2" onchange="cuandoCambia(this);">P<br>
            <input type="radio" name="a1-pro-1" value="3" onchange="cuandoCambia(this);" checked>MP 
        </td>
        <?php } ?>
    </tr>
    <tr class="a1tr">
        <td><input type="button" value="Añadir alternativa" name="a1" onclick="addElemento(this)"/></td>
        <td><input type="button" value="Eliminar alternativa" name="a1-d" onclick="removeElemento(this)"/></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="a1sep">
        <td colspan="4"><div class="separator"></div></td>
    </tr>
</table>

<table border="1" class="a2tab">
    <tr >
        <td colspan="4"><h3>Subproblema 2</h3></td>
    </tr>
    <tr>
        <td><b>Descripción: <?php echo $subproblemas[1]->getDescripcion();?></b></td>
        <td colspan="3"><b>Objetivos: <?php echo $subproblemas[1]->getObjetivo();?></b></td>
    </tr>
    <tr>
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
     <?php 
            $alternativas2 = $alternativaDAO->getAlternativasBySubproblema($subproblemas[1]->getId());
            
            if(sizeof($alternativas2)>0){
                $aux=1;
                foreach($alternativas2 as $alt){
                echo '<tr class="a2-d'.$aux.'">';
                echo '<td><textarea rows="3" cols="18" name="a2-alt'.$aux.'" class="a2-alt'.$aux.'" onchange="cuandoCambia(this);" required>'.$alt->getDescripcion().'</textarea></td>';
                echo '<td><textarea rows="3" cols="18" name="a2-con'.$aux.'" class="a2-alt'.$aux.'" required>'.$alt->getConsecuencia().'</textarea></td>';
                echo '<td>';
                for($i=1 ; $i<4 ; $i++){
                    $str="";
                    $symbol="";
                    switch ($i){
                        case 1:
                            $symbol="-";
                            break;
                        case 2:
                            $symbol="+";
                            break;
                        case 3:
                            $symbol="++";
                            break;
                    }
                    if($alt->getValoracion()==$i)
                        $str="checked";
                    echo '<input type="radio" name="a2-val-'.$aux.'" value="'.$i.'" onchange="cuandoCambia(this);" '.$str.'>'.$symbol.'<br>';
                }
                echo '</td><td>';
                for($i=1 ; $i<4 ; $i++){
                    $str="";
                    $symbol="";
                    switch ($i){
                        case 1:
                            $symbol="PP";
                            break;
                        case 2:
                            $symbol="P";
                            break;
                        case 3:
                            $symbol="MP";
                            break;
                    }
                    if($alt->getValoracion()==$i)
                        $str="checked";
                    echo '<input type="radio" name="a2-pro-'.$aux.'" value="'.$i.'" onchange="cuandoCambia(this);" '.$str.'>'.$symbol.'<br>';
                }
                echo '</td></tr>'; 
                $aux++;
                }
            }else{
        ?>
    <tr class="a2-d1">
        <td><textarea rows="3" cols="18" name="a2-alt1" class="a2-alt1" onchange="cuandoCambia(this);" required></textarea></td>
        <td><textarea rows="3" cols="18" name="a2-con1" class="a2-con1" required></textarea></td>
        <td>
            <input type="radio" name="a2-val-1" value="1" onchange="cuandoCambia(this);">-<br>
            <input type="radio" name="a2-val-1" value="2" onchange="cuandoCambia(this);">+<br>
            <input type="radio" name="a2-val-1" value="3" onchange="cuandoCambia(this);" checked>++ 
        </td>
        <td>
            <input type="radio" name="a2-pro-1" value="1" onchange="cuandoCambia(this);">PP<br>
            <input type="radio" name="a2-pro-1" value="2" onchange="cuandoCambia(this);">P<br>
            <input type="radio" name="a2-pro-1" value="3" onchange="cuandoCambia(this);" checked>MP 
        </td>
    </tr>
    <?php } ?>
    <tr class="a2tr">
        <td><input type="button" value="Añadir alternativa" name="a2" onclick="addElemento(this)"/></td>
        <td><input type="button" value="Eliminar alternativa" name="a2-d" onclick="removeElemento(this)"/></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="a2sep">
        <td colspan="4"><div class="separator"></div></td>
    </tr>
</table>

<table border="1" class="a3tab">
    <tr >
        <td colspan="4"><h3>Subproblema 3</h3></td>
    </tr>
    <tr>
        <td><b>Descripción: <?php echo $subproblemas[2]->getDescripcion();?></b></td>
        <td colspan="3"><b>Objetivos: <?php echo $subproblemas[2]->getObjetivo();?></b></td>
    </tr>
    <tr>
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
     <?php 
            $alternativas3 = $alternativaDAO->getAlternativasBySubproblema($subproblemas[2]->getId());
            
            if(sizeof($alternativas3)>0){
                $aux=1;
                foreach($alternativas3 as $alt){
                echo '<tr class="a3-d'.$aux.'">';
                echo '<td><textarea rows="3" cols="18" name="a3-alt'.$aux.'" class="a3-alt'.$aux.'" onchange="cuandoCambia(this);" required>'.$alt->getDescripcion().'</textarea></td>';
                echo '<td><textarea rows="3" cols="18" name="a3-con'.$aux.'" class="a3-alt'.$aux.'" required>'.$alt->getConsecuencia().'</textarea></td>';
                echo '<td>';
                for($i=1 ; $i<4 ; $i++){
                    $str="";
                    $symbol="";
                    switch ($i){
                        case 1:
                            $symbol="-";
                            break;
                        case 2:
                            $symbol="+";
                            break;
                        case 3:
                            $symbol="++";
                            break;
                    }
                    if($alt->getValoracion()==$i)
                        $str="checked";
                    echo '<input type="radio" name="a3-val-'.$aux.'" value="'.$i.'" onchange="cuandoCambia(this);" '.$str.'>'.$symbol.'<br>';
                }
                echo '</td><td>';
                for($i=1 ; $i<4 ; $i++){
                    $str="";
                    $symbol="";
                    switch ($i){
                        case 1:
                            $symbol="PP";
                            break;
                        case 2:
                            $symbol="P";
                            break;
                        case 3:
                            $symbol="MP";
                            break;
                    }
                    if($alt->getValoracion()==$i)
                        $str="checked";
                    echo '<input type="radio" name="a3-pro-'.$aux.'" value="'.$i.'" onchange="cuandoCambia(this);" '.$str.'>'.$symbol.'<br>';
                }
                echo '</td></tr>'; 
                $aux++;
                }
            }else{
        ?>
    <tr class="a3-d1">
        <td><textarea rows="3" cols="18" name="a3-alt1" class="a3-alt1" onchange="cuandoCambia(this);" required></textarea></td>
        <td><textarea rows="3" cols="18" name="a3-con1" class="a3-con1" required></textarea></td>
        <td>
            <input type="radio" name="a3-val-1" value="1" onchange="cuandoCambia(this);">-<br>
            <input type="radio" name="a3-val-1" value="2" onchange="cuandoCambia(this);">+<br>
            <input type="radio" name="a3-val-1" value="3" onchange="cuandoCambia(this);" checked>++ 
        </td>
        <td>
            <input type="radio" name="a3-pro-1" value="1" onchange="cuandoCambia(this);">PP<br>
            <input type="radio" name="a3-pro-1" value="2" onchange="cuandoCambia(this);">P<br>
            <input type="radio" name="a3-pro-1" value="3" onchange="cuandoCambia(this);" checked>MP 
        </td>
    </tr>
    <?php } ?>
    <tr class="a3tr">
        <td><input type="button" value="Añadir alternativa" name="a3" onclick="addElemento(this)"/></td>
        <td><input type="button" value="Eliminar alternativa" name="a3-d" onclick="removeElemento(this)"/></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="a3sep">
        <td colspan="4"><div class="separator"></div></td>
    </tr>
</table>
<input type="hidden" value="<?php echo $situacion->getId();?>" name="situacion" />

<p>Para cada uno de los sub-problemas y teniendo en cuenta la valoración y probabilidad de ocurrencia, las alternativas seleccionadas que usted prondrá en practica en la siguiente fase son:</p>
<table class="resumen">
</table>
<script type="text/javascript"> 
    llenarHTML();
</script>
<?php }?>