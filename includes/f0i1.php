<?php 
        header ('Content-type: text/html; charset=utf-8');
	
        $idUsuario= $_SESSION['user'];
	
	$encuestaDAO = new encuestaDAO();
	
	//Se quema la encuesta, se puede cambiar si se van a utilizar mas de una
	//encuesta
	$encuestaId=1;
	$encuesta= $encuestaDAO->getEncuesta($encuestaId);
?>

<script type="text/javascript">
	function guardar(index, idEncuesta, idPregunta){
        var values="p="+idPregunta+"&e="+idEncuesta+"&v="+$("input[name='group"+index+"']:checked").val();
        console.log(values);
        $.ajax({
               url: "../soluciondeproblemas/includes/guardarRespuesta.php",
               type: "post",
               data: values,
               success: function(data){
                       var res = JSON.parse(data);
                       console.log(res.message);
               },
               error:function(){
                       ok=false;
                       alert("Algo estuvo mal, por favor inicie otra vez el proceso o comuníquese con el administrador");
               }
            });	
        }
	
	
	/**
	* Devuelve un array con los valores de cada pregunta
	**/
	function getCheckedRadioButtons(){
		var checketRB = new Array();
		var x = document.getElemetnById("encuesta").rows.length;
		alert(x);
	}
	
</script>


<h2>Cuestionario:<?php echo $encuesta->getTitulo(); ?></h2>
<p>Responde las siguientes preguntas tipo selección múltiple con única respuesta</p>

<div class="datagrid">
	<table>
		<thead>
			<tr>
				<th>Valor</th>
				<th>Significado</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>0</td>
				<td>No es cierto en absoluto</td>
			</tr>
			<tr class="alt">
				<td>1</td>
				<td>Un poco cierto</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Bastante cierto</td>
			</tr>
			<tr class="alt">
				<td>3</td>
				<td>Muy cierto</td>
			</tr>
			<tr>
				<td>4</td>
				<td>Extremadamente cierto</td>
			</tr>
		</tbody>
	</table>
</div>
<br>

<?php 

	//verificar si ha realizado encuestas
	
	if($encuestaDAO->getEncuestasByUsuario($idUsuario)>0){
		//existe y se debe mostrar
                
                
	}else{
		
		//Se crea una encuesta nueva para el usuario
		if($encuestaDAO->crearEncuestaParaUsuario($idUsuario,$encuestaId)){
                        $encuestaUsuario = new UsuarioEncuesta();
                        $encuestaUsuario = $encuestaDAO->getUsuarioEncuestaByUsuario($idUsuario);
                        
			$encuesta= $encuestaDAO->getEncuesta($encuestaId);
			$lista = $encuestaDAO->getPreguntas($encuesta->getId());
			echo '<div class="datagrid"><table>
			<thead>
			<tr>
			<th></th>
			<th>0</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
                        <th>Acción</th>
			</tr>
			</thead>
			<tbody id="encuesta">';
	
			for($i = 0; $i < sizeof($lista) ; $i++){
				$alt="";
				if($i%2==0){
					$alt="alt";
				}
				echo "<tr class='".$alt."'><td>".($i+1).". ".$lista[$i]->getTexto()."</td>";
				for($j=0; $j<5; $j++){
					echo '<td><input type="radio" name="group'.$i.'" value="'.$j.'"></td>';
				}
                                echo "<td><input type='button' class='save_button' onclick='guardar(".$i.",".$encuestaUsuario->getId().",".$lista[$i]->getId().")'/></td></tr>";
                                
                                
			}
			echo '</tbody></table>';
			
		}else{
			echo "<h2>No se pudo crear la encuesta, consulte al administrador</h2>";
		}
	}

?>