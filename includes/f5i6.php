<?php 
header ('Content-type: text/html; charset=utf-8');

$idUsuario= $_SESSION['user'];

$situacionDAO = new situacionDAO();
$situacion= $situacionDAO->getSelectedSituacion($idUsuario);

$subproblemaDAO = new SubproblemaDAO();
$subproblemas= $subproblemaDAO->getSubproblemasBySituacion($situacion->getId());

$alternativaDAO = new alternativaDAO();
?>

<script type="text/javascript">
    function addElemento(e){
        var nombre=e.name;
        var nombreBoton = "."+e.name+"tr";
        var nombreTabla= "."+e.name+"tab";

        var $oldTab= $(document).find(nombreTabla);
        var $boton= $(document).find(nombreBoton);
        var $sep= $(document).find("."+e.name+"sep");

        var num = $(nombreTabla+" .new").length+1;

        var disable='disabled';
        var disableNum=$(nombreTabla+" tr").length-5;
        
        
        if(disableNum>=1){
            disable="";
        }
        
        
        $sep.remove();
        $boton.remove();
        $oldTab.append('<tr class="'+nombre+'-d'+num+' new"><td><textarea rows="3" cols="18" name="'+nombre+'-alt'+num+'" class="'+nombre+'-alt'+num+'" onchange="cuandoCambia(this);" required></textarea></td><td><textarea rows="3" cols="18" name="'+nombre+'-con'+num+'" class="'+nombre+'-con'+num+'" required></textarea></td><td><input type="radio" name="'+nombre+'-val-'+num+'" value="1"  onchange="cuandoCambia(this);">-<br><input type="radio" name="'+nombre+'-val-'+num+'" value="2" onchange="cuandoCambia(this);">+<br><input type="radio" name="'+nombre+'-val-'+num+'" value="3"  onchange="cuandoCambia(this);" checked>++ </td><td><input type="radio" name="'+nombre+'-pro-'+num+'" value="1"  onchange="cuandoCambia(this);">PP<br><input type="radio" name="'+nombre+'-pro-'+num+'" value="2"  onchange="cuandoCambia(this);">P<br><input type="radio" name="'+nombre+'-pro-'+num+'" value="3"  onchange="cuandoCambia(this);" checked>MP</td></tr><tr class="'+nombre+'tr enabled"><td><input type="button" value="Añadir alternativa" name="'+nombre+'" onclick="addElemento(this)"/></td><td><input type="button" value="Eliminar alternativa" name="'+nombre+'-d" onclick="removeElemento(this)" '+disable+'/></td><td></td><td></td></tr><tr class="'+nombre+'sep enabled"><td colspan="4"><div class="separator"></div></td></tr>');
        
    }
    
    function removeElemento(e){
        //hacer split del nombre
        var str=e.name;
        var nombre=str.split("-");
        var nombreTabla= "."+nombre[0]+"tab";

        var disableNum= $(nombreTabla+' .new').length;
        var $tr= $(document).find("."+nombre[0]+"-d"+disableNum);
        if(disableNum>1){
            $tr.remove();
            llenarHTML();
        }else{
            alert("Debe haber al menos una alternativa");
        }
    }
    
    function cuandoCambia(e){
        if(e.type=="radio"){
            var nombre=e.name.split('-');
            var value=$('input:radio[name='+e.name+']:checked').val();  
            if(nombre[1]=="val"){
                $('input:radio[name='+nombre[0]+'-pro-'+nombre[2]+']')[value-1].checked = true;
            }else{
                
                $('input:radio[name='+nombre[0]+'-val-'+nombre[2]+']')[value-1].checked = true;
            }
        }
        llenarHTML();
    }
    
    function llenarHTML(){
        var $resumen= $(document).find(".resumen");
        $resumen.empty();
        var text="<tr><td>";
        for(var i=1;i<4;i++){
            text+="<ul><li><b></b></li>";
            //var count= $(".a"+i+"tab tr").length-4;
            var ordenados=getValues(i);
            for(var j=1 ;j<ordenados.length+1 && j<4; j++){
                text+="<li><input type='hidden' value='"+ordenados[j-1][2]+"-"+ordenados[j-1][0]+"' name='hidden-"+i+"-"+j+"'/></li>";
            }
            text+="</ul>";
        }
        text+="</td></tr>";
        $resumen.append(text);
    }
    
    function getValues(subproblema){
        var cantidadTr= $('.a'+subproblema+'tab  .new').length;

        
        //1. traer todos los valores de un subproblema
        // y almacenarlos en una matriz [prioridad][texto]
        var prioridad=new Array();
        
        for(var i=0; i<cantidadTr; i++){
            prioridad[i]=new Array();
            prioridad[i][0]=parseInt($('input:radio[name=a'+subproblema+'-val-'+(i+1)+']:checked').val());
            prioridad[i][1]=$(".a"+subproblema+"-alt"+(i+1)).val();
            prioridad[i][2]=subproblema+"-"+(i+1);
        }
        //2. ordenar los valores de acuerdo a la prioridad
        prioridad.sort(function(a,b){
            return b[0] - a[0];
        });
        
        return prioridad
    }
    
</script>

<?php 
if(isset($_GET['err1'])){
    echo '<p class="message-box-error"><strong>Error</strong> - No deben haber cuadros en blanco</p>';
}
?>
<h2>Implementación de alternativas</h2>


<p><b>Confrontación consigo mismo: </b>Autoregistro de la implementación de alternativas (Identificación de las dificultades y aciertos en la puesta en práctica de las propuestas de solución a cada uno de los sub-problemas).</p>

<blockquote class="blue">
  <p><b>Problema identificado : </b><?php echo $situacion->getDescripcion();?></p>
</blockquote>

<table border="1" class="a1tab">
    <?php 
    $situacion= $situacionDAO->getSelectedSituacion($idUsuario);
    //traer los subproblemas asociados a la situacion
    $subproblemas=$subproblemaDAO->getSubproblemasBySituacion($situacion->getId());
    ?>
    <tr class="enabled">
        <td colspan="4"><h4>Subproblema 1:</h4>  <?php echo $subproblemas[0]->getDescripcion(); ?></td>
    </tr>
    <tr class="enabled">
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    <?php 
        $alternativas = $alternativaDAO->getAlternativasBySubproblema($subproblemas[0]->getId());
        
        foreach($alternativas as $alternativa){
            echo "<tr>";
                echo "<td>".$alternativa->getDescripcion()."</td>";
                echo "<td>".$alternativa->getConsecuencia()."</td>";
                echo "<td>".$alternativaDAO->getValoracion($alternativa->getValoracion())."</td>";
                echo "<td>".$alternativaDAO->getProbabilidad($alternativa->getProbabilidad())."</td>";
            echo "</tr>";
        }
        
    ?>
    <tr class="enabled">
        <td><b>Autoregistro de la implementacion de alternativas al subproblema 1:</b></td>
        <td colspan="3"><textarea rows="3" cols="53" name="auto1" class="auto1" required><?php echo $subproblemas[0]->getAutoregistro();?></textarea></td>
    </tr>
    <tr><td colspan="4"><p>En caso de no lograr cambios en el subproblema con las alternativas seleccionadas previamente, 
        puedes volver a seleccionar nuevas alternativas y ponerlas en práctica, continuando con el 
        Autoregistro de la implementación de alternativas.</p></td></tr>
    <tr class="enabled">
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    <tr class="a1-d1 new">
        <td><textarea rows="3" cols="18" name="a1-alt1" class="a1-alt1" onchange="cuandoCambia(this);"></textarea></td>
        <td><textarea rows="3" cols="18" name="a1-con1" class="a1-alt1"></textarea></td>
        <td>
            <input type="radio" name="a1-val-1" value="1" onchange="cuandoCambia(this);">-<br>
            <input type="radio" name="a1-val-1" value="2" onchange="cuandoCambia(this);">+<br>
            <input type="radio" name="a1-val-1" value="3" onchange="cuandoCambia(this);" checked>++ 
        </td>
        <td>
            <input type="radio" name="a1-pro-1" value="1" onchange="cuandoCambia(this);">PP<br>
            <input type="radio" name="a1-pro-1" value="2" onchange="cuandoCambia(this);">P<br>
            <input type="radio" name="a1-pro-1" value="3" onchange="cuandoCambia(this);" checked>MP 
        </td>
    </tr>
    <tr class="a1tr enabled">
        <td><input type="button" value="Añadir alternativa" name="a1" onclick="addElemento(this)"/></td>
        <td><input type="button" value="Eliminar alternativa" name="a1-d" onclick="removeElemento(this)"/></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="a1sep enabled">
        <td colspan="4"><div class="separator"></div></td>
    </tr>
</table>

<table border="1" class="a2tab">
    <tr class="enabled">
        <td colspan="4"><h4>Subproblema 2:</h4>  <?php echo $subproblemas[1]->getDescripcion(); ?></td>
    </tr>
    <tr class="enabled">
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    <?php 
        $alternativas2 = $alternativaDAO->getAlternativasBySubproblema($subproblemas[1]->getId());
        
        foreach($alternativas2 as $alternativa){
            echo "<tr>";
                echo "<td>".$alternativa->getDescripcion()."</td>";
                echo "<td>".$alternativa->getConsecuencia()."</td>";
                echo "<td>".$alternativaDAO->getValoracion($alternativa->getValoracion())."</td>";
                echo "<td>".$alternativaDAO->getProbabilidad($alternativa->getProbabilidad())."</td>";
            echo "</tr>";
        }
    ?>
    <tr class="enabled">
        <td><b>Autoregistro de la implementacion de alternativas al subproblema 2:</b></td>
        <td colspan="3"><textarea rows="3" cols="53" name="auto2" class="auto2" required><?php echo $subproblemas[1]->getAutoregistro();?></textarea></td>
    </tr>
    <tr><td colspan="4"><p>En caso de no lograr cambios en el subproblema con las alternativas seleccionadas previamente, 
        puedes volver a seleccionar nuevas alternativas y ponerlas en práctica, continuando con el 
        Autoregistro de la implementación de alternativas.</p></td></tr>
    <tr class="enabled">
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    <tr class="a2-d1 new">
        <td><textarea rows="3" cols="18" name="a2-alt1" class="a2-alt1" onchange="cuandoCambia(this);"></textarea></td>
        <td><textarea rows="3" cols="18" name="a2-con1" class="a2-con1"></textarea></td>
        <td>
            <input type="radio" name="a2-val-1" value="1" onchange="cuandoCambia(this);">-<br>
            <input type="radio" name="a2-val-1" value="2" onchange="cuandoCambia(this);">+<br>
            <input type="radio" name="a2-val-1" value="3" onchange="cuandoCambia(this);" checked>++ 
        </td>
        <td>
            <input type="radio" name="a2-pro-1" value="1" onchange="cuandoCambia(this);">PP<br>
            <input type="radio" name="a2-pro-1" value="2" onchange="cuandoCambia(this);">P<br>
            <input type="radio" name="a2-pro-1" value="3" onchange="cuandoCambia(this);" checked>MP 
        </td>
    </tr>
    <tr class="a2tr enabled">
        <td><input type="button" value="Añadir alternativa" name="a2" onclick="addElemento(this)"/></td>
        <td><input type="button" value="Eliminar alternativa" name="a2-d" onclick="removeElemento(this)"/></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="a2sep enabled">
        <td colspan="4"><div class="separator"></div></td>
    </tr>
</table>

<table border="1" class="a3tab">
    <tr class="enabled">
        <td colspan="4"><h4>Subproblema 3:</h4>  <?php echo $subproblemas[2]->getDescripcion(); ?></td>
    </tr>
    <tr class="enabled">
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    <?php 
        $alternativas3 = $alternativaDAO->getAlternativasBySubproblema($subproblemas[2]->getId());
        
        foreach($alternativas3 as $alternativa){
            echo "<tr>";
                echo "<td>".$alternativa->getDescripcion()."</td>";
                echo "<td>".$alternativa->getConsecuencia()."</td>";
                echo "<td>".$alternativaDAO->getValoracion($alternativa->getValoracion())."</td>";
                echo "<td>".$alternativaDAO->getProbabilidad($alternativa->getProbabilidad())."</td>";
            echo "</tr>";
        }
    ?>
    <tr class="enabled">
        <td><b>Autoregistro de la implementacion de alternativas al subproblema 3:</b></td>
        <td colspan="3"><textarea rows="3" cols="53" name="auto3" class="auto3" required><?php echo $subproblemas[2]->getAutoregistro();?></textarea></td>
    </tr>
    <tr><td colspan="4"><p>En caso de no lograr cambios en el subproblema con las alternativas seleccionadas previamente, 
        puedes volver a seleccionar nuevas alternativas y ponerlas en práctica, continuando con el 
        Autoregistro de la implementación de alternativas.</p></td></tr>
    <tr class="enabled">
        <td>Alternativas</td>
        <td>Consecuencias</td>
        <td>Valoración</td>
        <td>Probabilidad de ocurrencia</td>
    </tr>
    <tr class="a3-d1 new">
        <td><textarea rows="3" cols="18" name="a3-alt1" class="a3-alt1" onchange="cuandoCambia(this);"></textarea></td>
        <td><textarea rows="3" cols="18" name="a3-con1" class="a3-con1"></textarea></td>
        <td>
            <input type="radio" name="a3-val-1" value="1" onchange="cuandoCambia(this);">-<br>
            <input type="radio" name="a3-val-1" value="2" onchange="cuandoCambia(this);">+<br>
            <input type="radio" name="a3-val-1" value="3" onchange="cuandoCambia(this);" checked>++ 
        </td>
        <td>
            <input type="radio" name="a3-pro-1" value="1" onchange="cuandoCambia(this);">PP<br>
            <input type="radio" name="a3-pro-1" value="2" onchange="cuandoCambia(this);">P<br>
            <input type="radio" name="a3-pro-1" value="3" onchange="cuandoCambia(this);" checked>MP 
        </td>
    </tr>
    <tr class="a3tr enabled">
        <td><input type="button" value="Añadir alternativa" name="a3" onclick="addElemento(this)"/></td>
        <td><input type="button" value="Eliminar alternativa" name="a3-d" onclick="removeElemento(this)"/></td>
        <td></td>
        <td></td>
    </tr>
    <tr class="a3sep enabled">
        <td colspan="4"><div class="separator"></div></td>
    </tr>
</table>
<input type="hidden" value="<?php echo $situacion->getId();?>" name="situacion" />
<table class="resumen">
</table>
