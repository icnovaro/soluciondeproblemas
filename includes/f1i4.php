<h2>Solución de Problemas e Implementación de la Solución</h2>
<p>Sin dejar el medio ambiente natural, la solución de problemas se define como el proceso cognitivo
    y conductual auto dirigido por el cual una persona, pareja o grupo intenta identificar o descubrir 
    soluciones efectivas para problemas específicos encontrados en la vida diaria. Específicamente, este 
    procesos cognitivo-conductual (a) pone a disposición una variedad de soluciones potencialmente eficaces 
    para un problema particular, y (b) aumenta la probabilidad de seleccionar la solución más eficaz de 
    entre las diversas alternativas. (D´Zurilla, E.J., Nezu, A.M. & Maydeu-Olivares, A. 2004)</p>
<p>o	De esta manera el problema o situación problema, se define como: cualquier situación de la vida 
    o tarea (actual o anticipada) que requiere una respuesta de funcionamiento adaptativa, pero aparentemente 
    no hay la disponibilidad inmediata de la respuesta efectiva de la persona o personas que se enfrentan 
    a la situación debido a la presencia de uno o más obstáculos. (D´Zurilla, E.J., Nezu, A.M. & Maydeu-Olivares, A. 2004)</p>
<p>Los obstáculos para la solución de problemas puede deberse a la novedad de las demandas, la ambigüedad, la imposibilidad 
    de predecir los resultados, la presencia de demandas que entran en conflicto, déficit de habilidades o carencia de 
    recursos. (Ruiz Fernández, María Ángeles; Díaz García, Marta Isabel; Villalobos Crespo, Arabella.2012)</p>
<p>Este proceso se interesa por cualquier clase de problemas sociales que afecten el funcionamiento del consultante, 
    incluidos problemas impersonales (económicos), personales (dependencias), intrapersonales (salud emocional), 
    interpersonales (conflictos familiares), comunitarios (conflictos laborales) y sociales (discriminación social).</p>
<p>o	En perspectiva de resolver una situación problema se entiende la solución como: Una situación de respuesta 
    o patrón de respuesta de afrontamiento específico (cognitivo o conductual) que es el producto o el resultado 
    del proceso de resolución de problemas cuando se aplica a una situación problemática específica. (D´Zurilla, 
    E.J., Nezu, A.M. & Maydeu-Olivares, A. 2004)</p>
<p>Una solución efectiva es aquella que consigue el objetivo del proceso de solución d eproblemas, maximizando a la vez 
    las consecuencias positivas y minimizando las negativas. Estas consecuencias incluyen los resultados sociales 
    y personales a corto, mediano y largo plazo. (Ruiz Fernández, María Ángeles; Díaz García, Marta Isabel; Villalobos 
    Crespo, Arabella.2012)</p>
<p>o	Distinción entre Solución de problemas e Implementación de la solución. Estos dos procesos son conceptualmente 
    diferentes y requieren diferentes conjuntos de habilidades. La Solución de problemas refiere al proceso de búsqueda 
    de soluciones a problemas específicos, mientras que la implementación de soluciones se refiere al proceso de llevar 
    a cabo estas soluciones en las situaciones problemáticas reales. Las habilidades de Solución de problemas se asume 
    que son generales, mientras las habilidades para la implementación de soluciones se espera que varíen a través de 
    situaciones y dependiendo del tipo de problema y de la solución concreta. (D´Zurilla, E.J., Nezu, A.M. & Maydeu-Olivares,
    A. 2004)</p>
<p>De aquí se sigue que sin son diferentes las habilidades que se necesitan para solucionar problemas, de las que se 
    requieren para implementar la solución, y las primeras son desarrolladas a través de la Terapia en Solución de 
    problemas, el consultante debe autoevaluar: ¿Hasta dónde ha desarrollado las habilidades específicas que le exige 
    la implementación de la solución al problema planteado?. En caso de no contar con las habilidades necesarias 
    requerirá de un entrenamiento en las mismas para lograr una solución efectiva del problema identificado.</p>