<h2>Autocontrol</h2> 
<p><b>Cuando una persona se controla a sí misma</b>, escoge un camino de acción, da la solución a un problema o procura un incremento en el autoconocimiento, está comportándose. (Skinner, B.F. 1953). Por tanto, los <b>comportamientos que puedan inhibir o disminuir la probabilidad de que ocurra una respuesta indeseada,</b> por la manipulación de las variables de las que es función (lo que da algún tipo de recompensa o ventaja lo solemos hacer más a menudo, y lo que da algún castigo o desventaja lo hacemos lo menos posible), son comportamientos de autocontrol.</p>
<p><b>El autocontrol parte de una situación de conflicto</b> en relación con una conducta determinada, conflicto que se origina en el hecho de  que <b>la conducta es seguida tanto de consecuencias positivas como de consecuencias negativas.</b> La persona fluctúa entre un acercarse y un alejarse del objetivo debido a dos posibles combinaciones de conflictos:</p>
    <ul class="list-style">
        <li class="circle-list"><p>Consecuencias positivas inmediatas pero negativas a largo plazo.</p></li>
        <li class="circle-list"><p><b>Consecuencias negativas inmediatas pero positivas a largo plazo.</b></p></li>
    </ul>
<p>De acuerdo a estas dos posibles combinaciones de conflictos se proponen las siguientes modalidades de cambio:</p>
    <ul class="list-style">
        <li class="circle-list"><p>Para consecuencias inmediatamente positivas pero negativas a largo plazo, se trata de reducir la probabilidad de que ocurra dicha respuesta. (resistir una tentación y no dar la respuesta esperada).</p></li>
        <li class="circle-list"><p>Para consecuencias inmediatamente negativas pero positivas a largo plazo, se trata de incrementar la probabilidad de que ocurra dicha respuesta. (soportar algo desagradable y dar la respuesta esperada). (Fliegel, Kunzel, Groeger, Schulte, Sorgatz, 1989)</p></li>
    </ul>

