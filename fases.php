<?php
include './php/DAO/faseDAO.php';
include './php/DAO/conectionDAO.php';
include './php/Entities/Fase.php';

if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}

$faseDAO = new faseDAO();
$listaFases= $faseDAO->getFasesByUsuario($_SESSION['user']);
$idUsuario= $_SESSION['user'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Fases</p>
      </div>
    </div>
    <!--SITE CONTENTS-->
    <div class="grid_12 ident-top-1">
      <h1 class="text-t-big">Fases para la solución de problemas<strong class="sub-heading"></strong></h1>
      <p><b>Objetivo:</b>Favorecer la orientación positiva y la solución racional de problemas.</p>
      <p><b>Meta de resultados finales:</b> Reducir las conductas de evitación e impulsividad en la solución de problemas</p>
    </div>
    <div class="clear"></div>
    <div class="grid_8">
        <table>       
                <?php 
                    for($i=0; $i<count($listaFases); $i++){
                      echo '<tr>';
                      $fase= $listaFases[$i];  
                        if($faseDAO->isEnabled($idUsuario, $fase->getNumero())==0)
                          echo '<td><img class="fleft ident-right-1" src="images/icons/'.$fase->getNumero().'.png" alt="" /></td>';
                        else
                          echo '<td><img class="fleft ident-right-1" src="images/icons/'.$fase->getNumero().'c.png" alt="" /></td>';
                      echo '<td><a class="link-name" href="fase.php?id='.$fase->getNumero().'">'.$fase->getTitulo().'</a></td>';
                      echo '</tr>';
                   }
                ?>
        </table>
    </div>
    <!--<div class="grid_4">
      <div class="ident-bot-2 ident-right-1"> 
        <?php 
            /*for($i=1; $i<count($listaFases); $i+=2){
              $fase= $listaFases[$i];
              if($faseDAO->isEnabled($idUsuario, $fase->getNumero())==0)
                echo '<img class="fleft ident-right-1" src="images/icons/'.$fase->getNumero().'.png" alt="" />';
              else
                echo '<img class="fleft ident-right-1" src="images/icons/'.$fase->getNumero().'c.png" alt="" />'; 
              echo '<div class="extra-wrap ident-bot-2"> <a class="link-name" href="fase.php?id='.$fase->getNumero().'">'.$fase->getTitulo().'</a>
                        <p>'.$fase->getDescripcion().'</p>
                    </div>';
            }*/
        ?>
      </div>
    </div>-->
    <div class="grid_4 aligncenter"> 
        <?php
            $faseActual= $faseDAO->getLastFase($idUsuario);
            
            if($faseActual!=null){
        ?>
        <h1 class="text-t-big ">Fase actual<strong class="sub-heading"><?php echo $faseActual->getTitulo();?></strong></h1><br><br>
        <h1 class="text-t-big big-text"><?php echo $faseActual->getNumero(); ?></h1><br><br><br><br>
        <span class="button-border ident-bot-2"><?php echo '<a href="fase.php?id='.$faseActual->getNumero().'&i=1" class="button-green">Entrar</a>'; ?></span>
        <?php }else{?>
        <h1 class="text-t-big ">Has terminado</h1><br><br>
	<!--<span class="button-border ident-bot-2"><a href="fase1.html" class="button-green">Reiniciar</a></span> -->   
        <?php }?>
    </div>
    <div class="clear"></div>
    <div class="separator"></div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<footer>

<footer>
  <div class="footer-2">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_12">
          <div class="policy">Un proyecto de <a href="#">Yefren Díaz López</a> Reservados los derechos 2013</div>
        </div>
      </div>
    </div>
  </div>
</footer>
</footer>
<script src="js/custom.js"></script>
</body>
</html>
