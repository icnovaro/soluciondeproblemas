﻿<?php
include './php/DAO/faseDAO.php';
include './php/DAO/conectionDAO.php';
include './php/Entities/Fase.php';

if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}

$idUsuario= $_SESSION['user'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Fases</p>
      </div>
    </div>
    <!--SITE CONTENTS-->
    <div class="grid_12 ident-top-1">
      <h1 class="text-t-big">C&oacute;digos de inscripci&oacute;n<strong class="sub-heading"></strong></h1>
    </div>
    <div class="grid_3">
        <h3>Lista de c&oacute;digos </h3>
        <ul class="list-style">
            <?php 
            $usuarioDAO= new UsuarioDAO();
            $lista= $usuarioDAO->getCodes();
            
            foreach($lista as $codigo){
                echo "<li class='arrow-list'>".$codigo."</li>";
            }
            ?>  
        </ul>
    </div>
    <div class="grid_3">
        <?php 
            if(isset($_GET['s'])){
                echo '<p class="message-box-success">Has ingresado el c&oacute;digo </p>';
            }else if(isset ($_GET['error1'])){
                echo '<p class="message-box-error">Se ha encontrado un error </p>';
            }
        ?>
        <h3>Agregar c&oacute;digo</h3>
        <form method="POST" action="php/codigoDO.php" name="formulario">
            <ul class="list-style">
              <li class="arrow-list"><input type="text" name="codigo" /></li>
              <li class="arrow-list"><input type="submit" value="Agregar" /></li>
            </ul>
        </form>
    </div>
    <div class="clear"></div>
    <div class="separator"></div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<footer>

<footer>
  <div class="footer-2">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_12">
          <div class="policy">Un proyecto de <a href="#">Yefren Díaz López</a> Reservados los derechos 2013</div>
        </div>
      </div>
    </div>
  </div>
</footer>
</footer>
<script src="js/custom.js"></script>
</body>
</html>
