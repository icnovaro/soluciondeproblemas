<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Registro</p>
      </div>
    </div>
    <div class="grid_7">
      <h2>Registrate</h2>
      <!-- contact form -->
      <div id="confirm">
        <?php
            if(isset($_GET['error0'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - Debes ingresar todos los datos! </p>';
            }
            if(isset($_GET['error1'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - Las claves no coinciden! </p>';
            }
            if(isset($_GET['error2'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - La clave debe tener al menos 8 caracteres! </p>';
            }
            if(isset($_GET['error3'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - El código no es valido! </p>';
            }
            if(isset($_GET['error4'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - El usuario ya existe en la Base de datos! </p>';
            }
            if(isset($_GET['error5'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - El Email ya existe en la Base de datos! </p>';
            }
            if(isset($_GET['error6'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - Ingresaste carácteres extraños, no de admiten tildes, puntos, etc.! </p>';
            }
            if(isset($_GET['ok'])){
                echo "";
                echo '<p class="message-box-success"><strong>Enhorabuena</strong> - Te has registrado satisfactoriamente! </p>';
            }
       ?>
        <form id="login" method="POST" action="php/registerDO.php">
            <table>
                <tr>
                    <td>Nombres:</td>
                    <td><input type="text" name="nombres" required/></td>
                </tr>
                <tr>
                    <td>Nombre de usuario:</td>
                    <td><input type="text" name="usuario" required/></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="email" name="email" required/></td>
                </tr>
                <tr>
                    <td>Clave:</td>
                    <td><input type="password" name="clave" required/></td>
                </tr>
                <tr>
                    <td>Repita su clave:</td>
                    <td><input type="password" name="clave2" required/></td>
                </tr>
                <tr>
                    <td>Código de registro:</td>
                    <td><input type="text" name="codigo" required/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Registrarme"/>
                    </td>
                </tr>
            </table>
        </form>
      </div>
      <!-- end contact form -->
    </div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<?php 
    include 'footer.php';
?>
<script src="js/custom.js"></script>
</body>
</html>
