<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Restauración</p>
      </div>
    </div>
    <div class="grid_7">
      <h2>Restaura tu contraseña</h2>
      <!-- contact form -->
      <div id="confirm">
        <?php
            if(isset($_GET['error0'])){
                echo '<p class="message-box-error"><strong>Error</strong> Debes ingresar un usuario o un email  </p>';
            }
            if(isset($_GET['error1'])){
                echo '<p class="message-box-error"><strong>Error</strong> Usuario o email no existen en la base de datos </p>';
            }
            if(isset($_GET['error2'])){
                echo '<p class="message-box-error"><strong>Error</strong> Hubo un error al enviar el correo electrónico, por favor comuníquese con el administrador. </p>';
            }
            if(isset($_GET['ok'])){
                echo '<p class="message-box-success"><strong>Bien</strong> Se ha enviado un email con la nueva contraseña </p>';
            }
       ?>
        <p>Ingresa tu nombre de usuario o email:</p>
        <form id="restauracion" method="POST" action="php/restauracionDO.php">
            <table>
                <tr>
                    <td>Usuario:</td>
                    <td><input type="text" name="usuario"/></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="text" name="email"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Restaurar"/>
                    </td>
                </tr>
            </table>
        </form>
      </div>
      <!-- end contact form -->
    </div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<?php 
    include 'footer.php';
?>
<script src="js/custom.js"></script>
</body>
</html>
