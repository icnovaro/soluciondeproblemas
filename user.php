<?php
include './php/DAO/faseDAO.php';
include './php/DAO/conectionDAO.php';
include './php/Entities/Fase.php';

if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
}

$idUsuario= $_SESSION['user'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Fases</p>
      </div>
    </div>
    <!--SITE CONTENTS-->
    <div class="grid_12 ident-top-1">
      <h1 class="text-t-big">Zona de usuarios<strong class="sub-heading"></strong></h1>
    </div>
    <div class="grid_3">
        <h3>Cambio de contraseña: </h3>
        <form name="cambio" action="php/changeDO.php" method="POST">
            <table>
                <?php 
                    echo "<tr>";
                        echo "<td colspan='2'>";
                        if(isset($_GET['err1'])){
                            echo '<p class="message-box-error">Debes ingresar todos los campos</p>';
                        }
                        if(isset($_GET['err2'])){
                            echo '<p class="message-box-error">La nueva contraseña no coincide</p>';
                        }
                        if(isset($_GET['err3'])){
                            echo '<p class="message-box-error">La nueva contraseña debe tener al menos 6 caracteres</p>';
                        }
                        if(isset($_GET['err4'])){
                            echo '<p class="message-box-error">La contraseña con coincide</p>';
                        }
                        if(isset($_GET['s'])){
                             echo '<p class="message-box-success">Contraseña cambiada correctamente</p>';
                        }
                        echo "</td>";
                    echo "</tr>";
                ?>
                <tr>
                    <td>Constraseña Actual:</td>
                    <td><input type="password" name="actual" required/></td>
                </tr>
                <tr>
                    <td>Nueva contraseña:</td>
                    <td><input type="password" name="nueva1" required/></td>
                </tr>
                <tr>
                    <td>Confirmar nueva contraseña:</td>
                    <td><input type="password" name="nueva2" required/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Cambiar" /></td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="clear"></div>
    <div class="separator"></div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<footer>

<footer>
  <div class="footer-2">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_12">
          <div class="policy">Un proyecto de <a href="#">Yefren Díaz López</a> Reservados los derechos 2013</div>
        </div>
      </div>
    </div>
  </div>
</footer>
</footer>
<script src="js/custom.js"></script>
</body>
</html>
