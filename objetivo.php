<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link type="text/css" href="skin/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span></p>
      </div>
    </div>
    <div class="wrapper">
      <div class="tagline">
         <p>En la sociedad del conocimiento el uso de las tecnologías de la comunicación social, 
          como elemento terapéutico en psicología cognitivo conductual, favorece la socialización 
          del conocimiento científico y las técnicas de la psicología (en este caso en los procesos 
          de solución de problemas) en quienes han nacido y crecido en la era digital, considerados 
          como nativos digitales; y en quienes en su edad adulta han accedido a dichas herramientas 
          tecnológicas, considerados como inmigrantes digitales. </p>

         <p>Buscando promover la salud (en personas de confesionalidades cristianas), entendida como 
          una forma de vida orientada hacia la salud optima y el bienestar en el que el cuerpo, la 
          mente y el espíritu están integrados por la persona para vivir la vida plenamente en 
          interacción con los Otros, la Naturaleza y Dios.</p>
      </div>
    </div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<footer>
  <div class="footer-2">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_12">
          <div class="policy">Un proyecto de <a href="#">Yefren Díaz López</a> Reservados los derechos 2013</div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="js/custom.js"></script>
</body>
</html>
