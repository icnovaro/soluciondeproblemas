<?php

include './php/DAO/itemDAO.php';
include './php/DAO/alternativaDAO.php';
include './php/DAO/faseDAO.php';
include './php/DAO/historiaDAO.php';
include './php/DAO/situacionDAO.php';
include './php/DAO/subproblemaDAO.php';
include './php/DAO/conectionDAO.php';
include './php/DAO/encuestaDAO.php';

include './php/Entities/Item.php';
include './php/Entities/Fase.php';
include './php/Entities/Historia.php';
include './php/Entities/Situacion.php';
include './php/Entities/Subproblema.php';
include './php/Entities/Alternativa.php';
include './php/Entities/Pregunta.php';
include './php/Entities/Encuesta.php';
include './php/Entities/UsuarioEncuesta.php';


if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

if( !isset($_SESSION['user']) ){
    header("location: ./index.php");
    exit;
} 
    
$idFase=$_GET['id'];
$idUsuario= $_SESSION['user'];
$itemDAO = new itemDAO();
$faseDAO = new faseDAO();
$listaItems= $itemDAO->getItemsByFase($idFase);

$fase=$faseDAO->getFaseById($idFase);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link type="text/css" href="skin/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->

<script type="text/javascript" >

    function reciclarProceso(){
        
        var r=confirm("¿Vas a reciclar el proceso?, si lo haces todos tus datos de la fase 3 en adelante serán borrados, si no has guardado el informe dale cancelar y guardalo en tu PC.");
        if (r==true)
          {
              window.location.href="php/reciclarDO.php";
          }
        else
          {
          x="You pressed Cancel!";
          }
    }
    
     function reiniciarProceso(){
        
        var r=confirm("¿Estás seguro que deseas reiniciar el proceso ?, si esto pasa podrás escoger otra situación de la fase 2.");
        if (r==true)
          {
              window.location.href="php/reiniciarDO.php";
          }
        else
          {
          x="You pressed Cancel!";
          }
    }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span><a href="fases.php">Fases</a><?php echo "Fase ".$fase->getNumero(); ?></p>
      </div>
    </div>
    <div class="clear"></div>
    <!--SITE CONTENTS-->
    <?php
        if($fase->getNumero()>0 && $faseDAO->isEnabled($idUsuario, $fase->getNumero()-1)==0){
           echo '<div class="wrapper">';
           echo '<div class="grid_12">';
           echo '<p class="message-box-error"><strong>Error</strong> - Debes completar las anteriores fases para continuar </p>';
           echo '</br> <a href="fases.php" class="button-red ident-bot-2">Volver</a>';
           echo '</div>';
           echo '</div>';
        }else{
    ?>
    
    <div class="wrapper">
      <div class="grid_12">
        <h1 class="text-t-big ident-bot-0"><?php echo $fase->getTitulo(); ?></h1>
        <?php
        if(isset($_GET['c']) || $faseDAO->isCompleted($idUsuario, $idFase)==true){
           echo '</br><p class="message-box-success"><strong>Excelente</strong> - Has terminado esta fase </p>';   
        }
        ?>
      </div>
    </div>
    <div class="clear"></div>
    <div class="separator"></div>
	
    <div class="wrapper">
      <div class="grid_3">
            <h3>Menú</h3>
            <ul class="list-style">
            <?php
                foreach ($listaItems as $item){
                    $icono="check-list";
                    $habilitado= $itemDAO->isEnabled( $idUsuario, $item->getNumero(),$idFase);
                    if($habilitado=="0")
                        $icono="circle-list";
                    echo '<li class="'.$icono.'"><a href="fase.php?id='.$idFase.'&i='.$item->getNumero().'">'.utf8_encode($item->getTitulo()).'</a></li>';
                }
                
                echo "<li><a href='informe.php' target='_blank'><img src='./images/file_pdf.png' />Informe PDF</a></li>";
                
                $faseActual= $faseDAO->getLastFase($idUsuario);
                if($faseActual==null){
                    echo '<li><input type="button" class="button-red ident-bot-2" value="Reciclar Proceso" onclick="reciclarProceso()"/>';
                    echo '<input type="button" class="button-red ident-bot-2" value="Reiniciar Proceso" onclick="reiniciarProceso()"/></li>';
                    $situacionDAO= new situacionDAO();
                    $situacionDAO->setEnd($idUsuario);
                    
                    
                }
            ?>
            </ul>
      </div>
      <div class="grid_9">
        <div class="layout-box">
            
                <form method="POST" action="">
                <?php 
                $aux=0;
                if(!isset($_GET['i'])){
                     header("location: ./fase.php?id=".$idFase."&i=1");
                     exit;
                }
                else{
                    if($_GET['i']>1 && $itemDAO->isEnabled($idUsuario, ($_GET['i']-1),$idFase)==0){
                        echo '<p class="message-box-error"><strong>Error</strong> - Debes completar los pasos anteriores antes de ver este contenido </p>';
                        $aux=1;
                    }
                    else{
                        include 'includes/f'.$idFase.'i'.$_GET['i'].'.php';
                    }
                }

                if(($_GET['i']>1 && $itemDAO->isEnabled($idUsuario, $_GET['i']-1,$idFase)==1 && $itemDAO->isEnabled($idUsuario, $_GET['i'],$idFase)==0) || ($_GET['i']==1 && $itemDAO->isEnabled($idUsuario, $_GET['i'],$idFase)==0)){                   
                ?>
                <input type="hidden" name="idUsuario" value="<?php echo $idUsuario;?>"/>
                <input type="hidden" name="idFase" value="<?php echo $idFase;?>" />
                <input type="hidden" name="idItem" value="<?php echo $_GET['i'];?>" />
                <input type="submit" class="button-red ident-bot-2" value="Completar"/>
            
            <?php      
                }else if($aux==0){ 
                    echo '</br><p class="message-box-success left-float"><strong>Bien</strong> - Ya realizaste este paso </p>'; 
                }
            ?>
        </form>
       </div>
      </div>
    </div>
   <?php }?>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<footer>
  <div class="footer-2">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_12">
          <div class="policy">Un proyecto de <a href="#">Yefren Díaz López</a> Reservados los derechos 2013</div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="js/custom.js"></script>
</body>
</html>
