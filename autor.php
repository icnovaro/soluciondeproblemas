<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link type="text/css" href="skin/jplayer.blue.monday.css" rel="stylesheet" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span></p>
      </div>
    </div>
    <div class="wrapper">
      <div class="tagline">
         <h1>YEFREN DIAZ LOPEZ</h1>
         <p>Candidato a Doctor en Psicología en la Universidad de la Laguna-España. Magister en Desarrollo Educativo y 
          Social de CINDE-UPN. Psicólogo de la Fundación Universitaria Konrad Lorenz. Licenciado en Teología de la 
          Pontificia Universidad Javeriana. Con experiencia en Asesoría Escolar, Trabajo comunitario desde el campo 
          de la Psicología Comunitaria y de la Marginación; Docente Universitario presencial y virtual (en las plataformas 
          Blackboard y Moodle) en las áreas de psicología comunitaria, psicología de los grupos e investigación cualitativa. 
          Autor del Software en Solución de Problemas Online.</p>
      </div>
    </div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<footer>
  <div class="footer-2">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_12">
          <div class="policy">Un proyecto de <a href="#">Yefren Díaz López</a> Reservados los derechos 2013</div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="js/custom.js"></script>
</body>
</html>
