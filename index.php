<?php
include_once './php/DAO/usuarioDAO.php';
include_once './php/DAO/conectionDAO.php';
include_once './php/Entities/Usuario.php';
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page1">
<!-- header -->
<?php 
    include 'header.php';
?>
<!-- end header -->
<div class="row-2">
  <div class="container_12">
    <div class="grid_12">
      <div class="flexslider">
        <ul class="slides">
          <li> <img src="images/icons/slide-1.png" alt="" />
            <div class="flex-caption">
              <div class="flex-bg">
                <div class="flex-border">
                  <div class="slide-text-1 ident-bot-1">Registrate</div>
                  <div class="slide-text-2">Ingresa los datos y has parte de la comunidad.</div>
                  <div class="signup-form-ctnr">
                      <form action="registro.php" method="POST">
                      <input type="submit" class="button-red home-form-btn" value="REGISTRO" />
                      </form>
                  <div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="wrapper">
      <div class="grid_4"> <a href="#">
        <div class="box1">
          <div class="imgs"> <img src="images/icons/1page_img1.png" alt="" class="img-1" /> <img src="images/icons/1page_img1-hover.png" alt="" class="img-2" /> </div>
          <h3>¿Qué hace la plataforma?</h3>
          <p>La plataforma virtual es el espacio que viabiliza el conocimiento y experiencia del Terapeuta a través del Programa: Terapia de Solución de Problemas, que sigue el diseño terapéutico que el psicólogo ha adaptado a la Internet, para facilitar que el Consultante pueda asumir con responsabilidad, libertad, autonomía y compromiso práctico su propio proceso de solución de problemas.</p>
        </div>
        </a> </div>
      <div class="grid_4"> <a href="#">
        <div class="box1">
          <div class="imgs"> <img src="images/icons/1page_img2.png" alt="" class="img-1" /> <img src="images/icons/1page_img2-hover.png" alt="" class="img-2" /> </div>
          <h3>¿Cómo lo hace?</h3>
          <p>A través de un usuario y clave personal, el Usuario tiene el control total del programa, y por tanto el acceso a todas las fases de la intervención en la Terapia de Solución de Problemas (Evaluación diagnóstica, Contenidos en audio y videos, Actividades e Informes), por el tiempo estimado previamente por el Terapeuta.</p>
        </div>
        </a> </div>
      <div class="grid_4"> <a href="#">
        <div class="box1">
          <div class="imgs"> <img src="images/icons/1page_img3.png" alt="" class="img-1" /> <img src="images/icons/1page_img3-hover.png" alt="" class="img-2" /> </div>
          <h3>¿Cómo se usa?</h3>
          <p> El tutorial le indicará de una forma sencilla y gráfica paso a paso el procedimiento a seguir. Una vez se aprende a navegar en el Programa, el avance terapéutico dependerá del ritmo del Consultante-Terapeuta, que podrá una vez finalizadas las diferentes fases de intervención terapéutica seleccionar y repetir los procedimientos que considere necesarios para la solución del problema identificado.</p>
        </div>
        </a> </div>
    </div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<?php 
    include 'footer.php';
?>
<script src="js/custom.js"></script>
</body>
</html>
