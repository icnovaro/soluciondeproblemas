<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
    ob_start();
    
    $content='<page backtop="10mm" backbottom="10mm" backleft="20mm" backright="20mm">';
    $content.=' <page_header>';
    $content.='     <table style="width: 100%; border: solid 1px black;">';
    $content.='         <tr>';
    $content.='             <td style="text-align: left;    width: 33%">Informe</td>';
    $content.='             <td style="text-align: center;  width: 34%">Solución de Problemas Online</td>';
    $content.='             <td style="text-align: right;    width: 33%">'.date('d/m/Y').'</td>';     
    $content.='         </tr>';
    $content.='     </table>';        
    $content.=' </page_header>';
    $content.='<page_footer>';
    $content.=' <table style="width: 80%; border: solid 1px black;">';
    $content.='     <tr>';
    $content.='         <td style="text-align: left;    width: 50%">Informe</td>';
    $content.='         <td style="text-align: right;    width: 50%">page [[page_cu]]/[[page_nb]]</td>';
    $content.='     </tr>';
    $content.=' </table>';    
    $content.='</page_footer>';    
    $content.='<span style="font-size: 22px; font-weight: bold">Informe de solución del problema</span><br><br><br>';
    $content.='<span style="font-size: 16px; font-weight: bold">1.Antecedentes del problema</span><br>';
    $content.='<br><br>';
    $content.='<table style="width: 100%;border: solid 1px #5544DD; border-collapse: collapse" align="left">';
    /*$content.=' <thead>';
    $content.='     <tr>';
    $content.='         <th style="width: 30%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Header 1</th>';
    $content.='         <th style="width: 30%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Header 2</th>';
    $content.='     </tr>';
    $content.=' </thead>';     */       
    $content.=' <tbody>';            


    $content.=' <tr>';
    $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;" >';
    $content.='         Descripción del problema que usted había solucionado desde su experiencia religiosa.';
    $content.='     </td>';
    $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='      TODO';
    $content.='     </td>';
    $content.=' </tr>';
    $content.=' <tr>';
    $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='         ¿Qué sucedió antes del problema?';
    $content.='     </td>';
    $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='      TODO';
    $content.='     </td>';
    $content.=' </tr>';
    $content.=' <tr>';
    $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='         ¿Qué cambios realizó para solucionar el problema?';
    $content.='     </td>';
    $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='      TODO';
    $content.='     </td>';
    $content.=' </tr>';
    $content.=' <tr>';
    $content.='     <td style="width: 40%; margin:0px; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='         ¿Cuáles fueron las consecuencias después de solucionar el problema?';
    $content.='     </td>';
    $content.='     <td style="width: 60%; text-align: left; border: solid 1px #55DD44; padding:5px;">';
    $content.='      TODO';
    $content.='     </td>';
    $content.=' </tr>';

    $content.=' </tbody>';
    /*$content.=' <tfoot>';
    $content.='     <tr>';
    $content.='         <th style="width: 30%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Footer 1</th>';
    $content.='         <th style="width: 30%; text-align: left; border: solid 1px #337722; background: #CCFFCC">Footer 2</th>';            
    $content.='     </tr>';            
    $content.=' </tfoot>'; */
    $content.='</table>';  
    $content.='<br>';
    $content.='Ca marche !!!<br>';
    $content.='refaisons un test : <br>';
    $content.='<table style="width: 80%;border: solid 1px #5544DD">';
    
    for ($k=0; $k<12; $k++) {
        
        $content.='<tr>';
        $content.=' <td style="width: 30%; text-align: left; border: solid 1px #55DD44">';
        $content.='     test de texte assez long pour engendrer des retours à la ligne automatique...';
        $content.=' </td>';
        $content.=' <td style="width: 70%; text-align: left; border: solid 1px #55DD44">';
        $content.='     test de texte assez long pour engendrer des retours à la ligne automatique...';        
        $content.=' </td>';
        $content.='</tr>';
    }
    $content.='</table>';
    $content.='<br>';
    $content.='Ca marche toujours !<br>';
    $content.='</page>';
    $content.='<page pageset="old">';
    $content.=' Nouvelle page !!!!';
    $content.='</page>';
    
    
    // convert to PDF
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 3);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple03.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
