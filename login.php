<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Login</p>
      </div>
    </div>
    <div class="grid_7">
      <h2>Inicia Sesión</h2>
      <!-- contact form -->
      <div id="confirm">
        <?php
          if(isset($_GET['error0'])){
              echo '<p class="message-box-error"><strong>Error</strong> - Debes ingresar todos los datos! </p>';
          }else if(isset($_GET['error1'])){
              echo '<p class="message-box-error"><strong>Error</strong> - Usuario o contraseña incorrectos! </p>';
          }
        ?>
        <form id="login" method="POST" action="php/loginDO.php"/>
          <fieldset>
          <label class="name">
          <input type="text" value="Usuario:" name="usuario"/>
          <span class="error">*Este no es un usuario valido.</span> <span class="empty">*Usuario .</span> </label>
          <label class="email">
          <input type="password" value="Clave:" name="clave"/>
          <span class="error">*Esta no es una clave válida.</span> <span class="empty">*Clave.</span> </label>
          <div class="clear"></div>
          <input type="submit" value="Entrar" />
          <p><a href="restauracion.php">¿Has olvidado tu contraseña?</a></p>
          </fieldset>
        </form>
      </div>
      <!-- end contact form -->
    </div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<?php 
    include 'footer.php';
?>
<script src="js/custom.js"></script>
</body>
</html>
