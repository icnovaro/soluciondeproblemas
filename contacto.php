<!DOCTYPE html>
<html lang="en">
<head>
<title>Terapia en Soluciones de Problemas ON-LINE</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0" />
<link rel="stylesheet" href="css/style.css" media="screen" />
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<script src="js/jquery-1.7.2.min.js"></script>
<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<link rel="stylesheet" href="css/ie.css"> 
	<![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="page-inner">
<!-- header -->
<?php
    include 'header.php';
?>
<!-- content -->
<section id="content">
  <div class="container_12">
    <div class="grid_12">
      <div class="breadcrumbs">
        <p><span class="bread-home"><a href="index.php">Inicio</a></span>Contacto</p>
      </div>
    </div>
    <div class="grid_7">
      <h2>Contáctenos</h2>
      <span>Skype: yefren.diaz</span><br>
        <span>Correo: yefrendiaz@hotmail.com</span>
      <!-- contact form -->
      <div id="confirm">
          
        <?php
            if(isset($_GET['error'])){
                echo "";
                echo '<p class="message-box-error"><strong>Error</strong> - Hubo un error, por favor intentalo mas tarde! </p>';
            }
            if(isset($_GET['ok'])){
                echo "";
                echo '<p class="message-box-success"><strong>Enhorabuena</strong> - Has enviado el mensaje, pronto te responderemos. </p>';
            }
        ?>
      </div>
      
       <h2>Déjanos un mensaje:</h2>
        <form id="contacto" method="POST" action="php/contactoDO.php">
        
            <table>
                <tr>
                    <td>Nombres:</td>
                    <td><input type="text" name="nombres" required/></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="email" name="email" required/></td>
                </tr>
                <tr>
                    <td>Mensaje:</td>
                    <td><textarea name="mensaje"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Enviar"/>
                    </td>
                </tr>
            </table>
        </form>
      </div>
      <!-- end contact form -->
    </div>
    <div class="clear"></div>
  </div>
</section>
<!-- end content -->
<!-- footer -->
<?php 
    include 'footer.php';
?>
<script src="js/custom.js"></script>
</body>
</html>
